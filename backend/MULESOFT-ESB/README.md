# PROYECTOS

## conexion_db
    Muestra como configurar una conexión a una base de datos mysql, donde se raliza una operación insert 
    y por el flujo de error se lleva a cabo una operación select.
   
## consumer_webservice
    Podremos ver como consumir un webservice, antes realizando una transformación 
    en el parámetro de entrada para consumir dicho servicio.
   
## createService
    Veremos como crear un webservice con el componente CXF de mulesoft utilizando la operacion JAX-WS service.
   
## ftp3
    Se muestra como configurar el componente ftp, para obtener archivos desde un servidor ftp, 
    luego copiarlos a una ruta determinada con el componente file de mulesoft.
    
## mail
    Muestra como enviar un mail desde un correo, usando el componenete SMTP de mulesoft.
    
## sopaservicemultinput
    Veremos como crear un webservice que recibe multiples parámetros en un objeto.

## mysql
     Muestra una conexion basica a mysql y utilizando un parametro para la query

## rest-consume-ml
    Muestra como consumir recursos rest de la api de mercado libre

## soap-comsume
    Muestra como consumir un soap webservice mediante servicios expuetos, pasando un lacation wsdl

## apessentials
    Muestra menejo de variables varios

## jms_activemq_test
    Muestra como conectarse mediante un componente jms (java message service) de mule esb a un servidor active mq para el manejo de colas de mensajes
    Este test se realiza con un servidor local de activemq
    1-Se descargo en local desde apache activemq
    2-Por defecto el user es: admin y el pass: admin
    3-Se descarga el zip, se descompirme y se guarda la carpeta en una ubicacion cualquiera(archivos de programas) 
    4-Se ingresa a la ubicacion ejemplo: C:\Program Files\apache-activemq-5.15.2\bin\win64 
    5-Se encontrara un .bat (activemq.bat)
    6-Le damos click y se iniciara el activemq en local ( se abrira un cmd indicando el puerto "8161" )
    7-Abrimos un browser y entramos a la consola de activemq: http://localhost:8161/admin/
    8-Mediante la consola podremos crear queues y topic, el nombre que le demos se usara en el proyecto y para la configuracion
      de jms de mule las credenciales antes mencionadas (user: admin, pass: admin)
    9-Ya tendrmos listo para probar el servicio de mensajes de la app de mule entrado al  postman por ejemplo y entrando al la app de mule 
      mediante su endpoint asignada el el componente http de mule

