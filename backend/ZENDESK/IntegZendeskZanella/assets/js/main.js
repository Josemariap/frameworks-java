$(function () {
    var client = ZAFClient.init();
    configuracion(client, function (config) {
        client.invoke('resize', {width: '400px', height: '500px'});
        $("#ticketForm").hide();
        $("#search_motorcycle").hide();
        mostrarFormularioPrincipal(client, config);
    })
});


function mostrarFormularioPrincipal(client, config) {
    inicioSessionZendesk(client, function (dataUser) {
        console.log("USER token: " + dataUser.user.authenticity_token)

        var idTicket;
        client.context().then(function (context) {
            console.log(context);
            idTicket = context.ticketId
        });


        client.context().then(function (context) {
            if (context.location == 'ticket_sidebar') {
                client.get('ticket.requester.name').then(function (data) {
                    console.log(data)
                    $("#info_moto").append('<p><h4 class="text-center"><strong>Solicitante del ticket</strong></h4></p><label>Nombre: ' + data["ticket.requester.name"] + ' </label><br>' +
                        '<label>Ticket id: ' + idTicket + ' </label><br>')
                })
            }
        });


        client.on('ticket.save', function () {
            console.log("Captura el evento lanzado por el ticket")
            return true;
        });


    })


    $("#add-btn").on("click", function (event) {
        event.preventDefault();
        $("#ticketForm").hide();
        if ($("#num_chassis").val().length == 0 || $("#num_engine").val().length == 0) {
            client.invoke('notify', 'Revise los campos', 'error');
            emptyFields();
        } else {
            searchMotorcycleApiRestZanella(client)
        }

    });
}

function inicioSessionZendesk(client, callback) {
    configuracion(client, function (config) {

        var settings = {
            url: config.url + '/api/v2/users/me.json',
            type: 'GET',
            headers: {
                'Authorization': 'Basic ' + btoa(config.username + ':' + config.password),
                'Content-Type': 'application/json'
            }
        };
        client.request(settings).then(function (data) {

            var dataJson = data;

            callback(dataJson);
        });
    });
}


function configuracion(client, callback) {
    client.context().then(function (context) {
        var subdomain = context.account.subdomain;
        var config = {
            url: 'https://' + subdomain + '.zendesk.com',
            username: login_zendesk.mail,
            password: login_zendesk.pass
        };
        callback(config);
    });
}


function crearTicket(client, config, token) {
    client.get('currentUser').then(function (userData) {
        var datos = {
            ticket: {
                subject: "Prueba",
                comment: "Haciendo una prueba desde la app",
                requester_id: 22263639687,
                assignee_id: userData.currentUser.id
            }

        };

        var peticion = {
            url: config.url + '/api/v2/tickets.json',
            type: 'POST',
            headers: {
                "X-CSRF-Token": token
            },
            data: datos
        };

        client.request(peticion).then(function (response) {
            client.invoke('notify', 'Ticket numero: ' + response.ticket.id + ' creado con exito');
        });
    })
}


function searchMotorcycleApiRestZanella(client) {
    emptyFields();
    var settings3 = {
        url: "https://api-rest-zanella-springboot.herokuapp.com/rest/motorcy" +
        "cle/prueba1575733/prueba7456878",
        type: 'GET',
        cors: true
    };

    client.request(settings3).then(function (response) {
        client.invoke('notify', 'Articulo encontrado con éxito')
        $("#info_moto").append('<p><h4 class="text-center"><strong>Motorcycle information:</strong></h4></p><label>Number motor: ' + response.numEngine + ' </label><br>' +
            '<label>Number chassis: ' + response.numChassis + ' </label><br>' +
            '<label>Holder: ' + response.holder + ' </label><br>' +
            '<label>Mail: ' + response.mail + ' </label><br>' +
            '<label>Patent: ' + response.patent + ' </label><br>' +
            '<label>Motorcycle model: ' + response.motorcycleModel + ' </label><br>'
        );
        console.log(response)
    })


}