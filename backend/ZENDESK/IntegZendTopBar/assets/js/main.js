$(function () {
    var client = ZAFClient.init();
    inicioSessionZendesk(client, function (dataJson) {
        console.log("DATA_JSON :  " + JSON.stringify(dataJson.user))
        console.log("TOKEN_USER_ZENDESK:  " + dataJson.user.authenticity_token )
        configuracion(client, function (config) {
            crearTicket(client, config, dataJson.user.authenticity_token)
        })

    })

});

function inicioSessionZendesk(client, callback) {
    configuracion(client, function (config) {

        var settings = {
            url: config.url + '/api/v2/users/me.json',
            type: 'GET',
            headers: {
                'Authorization': 'Basic ' + btoa(config.username + ':' + config.password),
                'Content-Type': 'application/json'
            }
        };
        client.request(settings).then(function (data) {

            var dataJson = data;

            callback(dataJson);
        });
    });
}


function configuracion(client, callback) {
    client.context().then(function (context) {
        var subdomain = context.account.subdomain;

        var config = {
            url: 'https://' + subdomain + '.zendesk.com',
            urlApi: "https://api-zendesk.herokuapp.com/usuario/",
            username: login_zendesk.mail,
            password: login_zendesk.pass
        };
        callback(config);
    });
}


function crearTicket(client, config, token){
    client.get('currentUser').then(function (userData) {
        var datos = {
            ticket: {
                subject: "Prueba",
                comment: "Haciendo una prueba desde la app",
                requester_id: 22263639687, 
                assignee_id: userData.currentUser.id
            }

        };

        var peticion = {
            url: config.url + '/api/v2/tickets.json',
            type: 'POST',
            headers: {
                "X-CSRF-Token": token
            },
            data: datos
        };

        client.request(peticion).then(function (response) {
            client.invoke('notify', 'Ticket numero: ' + response.ticket.id + ' creado con exito');
        });
    })
}




