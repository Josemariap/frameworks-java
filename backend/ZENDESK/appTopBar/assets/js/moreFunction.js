
function templateHide() {
    $("#user").hide();
    $("#userForm").hide();
    $("#ticketForm").hide();
    $("#ticket").hide();
    $('#userError').hide();

}

function vaciarCampos() {
    $("#dni").val("");
    $("#subject").val("");
    $("#desc").val("");
    $("#name").val("");
    $("#email").val("");
}


function validarEmail(emailAddress) {
    // si esta en blanco devuelve true
    var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
    return pattern.test(emailAddress);
}


function analizarErrorNuevoUsuarioZendesk(error) {
    console.log(error);
    if (error.details.name) {
        var data = {
            description: error.details.name[0].description,
            error: error.details.name[0].error
        };

        return data;
    }
    
    if (error.details.email) {
        var data = {
            description: error.details.email[0].description,
            error: error.details.email[0].error

        };

        return data;
    }
}

