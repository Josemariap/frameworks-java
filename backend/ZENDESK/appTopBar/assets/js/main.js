$(function () {
    var client = ZAFClient.init();
    configuracion(client, function (config) {
        client.invoke('resize', { width: '300px', height: '500px' });
        templateHide();
        mostrarFormularioPrincipal(client, config);
    });
});





function mostrarFormularioPrincipal(client, config) {
    $("#add-btn").on("click", function (event) {
        event.preventDefault();
        templateHide();
        if ($("#dni").val().length == 0) {
            client.invoke('notify', 'El campo DNI no puede estar en blanco', 'error');
        } else {
            var dni = $("#dni").val();
            if (!isNaN(dni)) {
                busquedaPorDni(dni, client, config);
            } else {
                client.invoke('notify', 'El campo DNI debe ser numerico', 'error');
            }
        }
        vaciarCampos();
    });
}

function busquedaPorDni(dni, client, config) {

    client.invoke('notify', 'Enviando peticion de busqueda de usuario');

    inicioSessionZendesk(client, function (token) {

        var settings2 = {
            url: config.url + '/api/v2/users/search.json?query=dni:' + dni + '',
            type: 'GET',
            headers: {
                "X-CSRF-Token": token
            },
        };

        client.request(settings2).then(function (response) {

            // el array users viene vacio si no encuentra.
            if (response.users.length != 0) {
                buscarUsuarioZendesk(response, client, config, token);
            } else {
                buscarUsuarioRest(dni, config, client, token);
            }

        }
        );
        fgfg
    });
    client.invoke('notify', 'Buscando Usuario... por favor espere');
}

function inicioSessionZendesk(client, callback) {
    configuracion(client, function (config) {

        var settings = {
            url: config.url + '/api/v2/users/me.json',
            type: 'GET',
            headers: {
                'Authorization': 'Basic ' + btoa(config.username + ':' + config.password),
                'Content-Type': 'application/json'
            }
        };
        client.request(settings).then(function (data) {

            var token = data.user.authenticity_token;

            callback(token);
        });
    });
}

function buscarUsuarioZendesk(response, client, config, token) {
    client.invoke('notify', 'Usuario encontrado en zendesk');

    var data = {
        titulo: "Usuario ubicado en DB Zendesk",
        name: response.users[0].name,
        dni: response.users[0].user_fields.dni
    };
    templatingHBS("user", "userInfo", data);
    $("#user").show();

    var userId = response.users[0].id;
    crearNuevoTicket(client, userId, config, token);
}

function buscarUsuarioRest(dni, config, client, token) {

    var settings3 = {
        url: config.urlApi + dni + '',
        type: 'GET',
        cors: true
    };

    client.request(settings3).then(function (response) {
        client.invoke('notify', 'Usuario encontrado en ApiRest y creado en Zendesk');

        crearUsuarioZendesk(response, client, config, token, function (usuario) {
            var userId = usuario.user.id;
            var data = {
                titulo: "Usuario importado a DB de Zendesk",
                name: usuario.user.name,
                dni: usuario.user.user_fields.dni
            };
            templatingHBS("user", "userInfo", data);
            $("#user").show();
            crearNuevoTicket(client, userId, config, token);
        });


    }).catch(function (error) {
        client.invoke('notify', 'El usuario no existe', 'error');
        $("#userForm").show();
        $("#createTicket").hide();
        $("#btn1").off("click").on("click", function () {
            client.invoke('notify', 'Aguarde, usuario creandose');

            $("#userForm").hide();
            var name = $("#name").val();
            var email = $("#email").val();

            if (validarEmail(email)) {

                var user = {
                    nombre: name,
                    email: email,
                    dni: dni,
                };

                crearUsuarioZendesk(user, client, config, token, function (usuario) {
                    client.invoke('notify', 'Usuario creado con exito');
                    var userId = usuario.user.id;
                    var data = {
                        titulo: "Usuario creado en DB de Zendesk",
                        name: usuario.user.name,
                        dni: usuario.user.user_fields.dni
                    };
                    templatingHBS("user", "userInfo", data);
                    $("#user").show();
                    crearNuevoTicket(client, userId, config, token);
                });

            } else {

                client.invoke('notify', 'Ingrese un Email valido', 'error');
                $("#userForm").show();

            }

        });

    });
}

function crearUsuarioZendesk(usuario, client, config, token, callback) {

    var datos = {
        user: {
            name: usuario.nombre,
            email: usuario.email,
            user_fields: {
                dni: usuario.dni
            }
        }
    };

    var settings4 = {
        url: config.url + '/api/v2/users.json',
        type: 'POST',
        headers: {
            "X-CSRF-Token": token
        },
        data: datos
    };

    client.request(settings4).then(function (response) {

        callback(response);

    }).catch(function (response) {

        $('#userError').show();

        var error = JSON.parse(response.responseText);
        var resultado = analizarErrorNuevoUsuarioZendesk(error);

        templatingHBS("userError", "userErrorInfo", resultado);

        client.invoke('notify', 'error de zendesk', 'error');

        $("#userForm").show();


    });
}

function crearNuevoTicket(client, usuarioId, config, token) {
    client.get('currentUser').then(function (userData) {
        $("#ticketForm").show();
        $('#userError').hide();


        $("#botonTicket").off("click").on("click", function () {
            templateHide();

            var subject = $("#subject").val();
            var desc = $("#desc").val();

            if (subject != "" && desc != "") {

                var datos = {
                    ticket: {
                        subject: subject,
                        comment: desc,
                        requester_id: usuarioId,
                        assignee_id: userData.currentUser.id
                    }

                };

                var peticion = {
                    url: config.url + '/api/v2/tickets.json',
                    type: 'POST',
                    headers: {
                        "X-CSRF-Token": token
                    },
                    data: datos
                };

                client.request(peticion).then(function (response) {

                    $("#ticket").show();
                    $("#user").hide();


                    client.invoke('notify', 'Ticket numero: ' + response.ticket.id);

                    var data = {
                        id: response.ticket.id
                    };

                    templatingHBS("ticket", "ticketInfo", data);

                    client.invoke('routeTo', 'ticket', data.id);

                    vaciarCampos();
                });

            } else {
                client.invoke('notify', 'Los campos del ticket no deben estar vacios', 'error');
                $("#ticketForm").show();

            }


        });

    });
}


function templatingHBS(contenedor, script, data) {
    var scrip = "#" + script;
    var source = $(scrip).html();
    var template = Handlebars.compile(source);
    var cont = "#" + contenedor;
    $(cont).html(template(data));
}

function configuracion(client, callback) {

    client.context().then(function (context) {
        var subdomain = context.account.subdomain;

        var config = {
            url: 'https://' + subdomain + '.zendesk.com',
            urlApi: "https://api-zendesk.herokuapp.com/usuario/",
            username: "lautaro.molinero@snoopconsulting.com",
            password: "snoop123"
        };

        callback(config);
    });
}

