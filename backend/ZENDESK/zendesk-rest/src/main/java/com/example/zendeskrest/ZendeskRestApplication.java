package com.example.zendeskrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zendesk.client.v2.Zendesk;
import sun.security.krb5.internal.Ticket;

@SpringBootApplication
public class ZendeskRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZendeskRestApplication.class, args);
	}
}
