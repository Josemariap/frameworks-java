package com.example.zendeskrest.controller;
import org.springframework.web.bind.annotation.*;
import org.zendesk.client.v2.Zendesk;

@RestController
public class TicketController {

     @RequestMapping(value = "/ticket/AssigneedId/{id}", method = RequestMethod.GET)
     public Long getTicketAssigneedIdById(@PathVariable Long id){

         Zendesk zd = new Zendesk.Builder("https://snooppruebas1493322597.zendesk.com")
                 .setUsername("jose.pico@snoopconsulting.com")
                 .setToken("IS0NcZOgaBJXz02LPlbYHi9JaRt65H6sSfAmIeWn") // or .setPassword("...")
                 .build();
         return  zd.getTicket(id).getAssigneeId();
     }
}
