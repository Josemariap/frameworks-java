package com.example.zendeskrest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.zendesk.client.v2.Zendesk;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TicketTest {

    @Test
    public void getAssigneedIdByIdTicket_returnLong() {
        Zendesk zd = new Zendesk.Builder("https://snooppruebas1493322597.zendesk.com")
                .setUsername("jose.pico@snoopconsulting.com")
                .setToken("IS0NcZOgaBJXz02LPlbYHi9JaRt65H6sSfAmIeWn") // or .setPassword("...")
                .build();
        Long assigneedId = zd.getTicket(65).getAssigneeId();
        Long expected = 23029786428L;
        assertEquals(expected, assigneedId);
    }

}
