package controllers;

import com.opensymphony.xwork2.ActionSupport;


//Clase controller/Action
public class Usuario extends ActionSupport{
	
	private String userlogin;
	private String userpsw;
	private String usernombre;
	
	
	public String getUserlogin() {
		return userlogin;
	}
	public void setUserlogin(String userlogin) {
		this.userlogin = userlogin;
	}
	public String getUserpsw() {
		return userpsw;
	}
	public void setUserpsw(String userpsw) {
		this.userpsw = userpsw;
	}
	public String getUsernombre() {
		return usernombre;
	}
	public void setUsernombre(String usernombre) {
		this.usernombre = usernombre;
	}
	
	//El metodo al llamarse "execute" es llamado de forma automatica cuando se llama a la clase desde el struts.xml
	public String execute(){
		String ir=ERROR;
		if(getUserlogin().equals("axl") && getUserpsw().equals("123")){
			setUsernombre("Axl Rose");
			ir=SUCCESS;
		}
		return ir;
	}
	
	

}
