package controllers;

import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ActionSupport;

public class SegundoAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
    
	private List <String> cursos;
	 
	private String titulo = "Probando Structs 2";

	
	public List<String> getCursos() {
		return cursos;
	}


	public void setCursos(List<String> cursos) {
		this.cursos = cursos;
	}


	public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public String execute() throws Exception {
		cursos = new ArrayList<String>();
		cursos.add("java");
		cursos.add("hibernate");
		cursos.add("spring");
		return SUCCESS;
	}

	
	
	
	
	
	
}
