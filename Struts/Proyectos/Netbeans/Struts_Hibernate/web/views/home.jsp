<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" /> 
        <title>struts2-Hibernate</title>
    </head>
    <body>
        <h1>Listado de usuarios</h1>
        
         <ul>
            
      
          <ul>
            <s:iterator value="datos"  var="valor" status="estado">
                <li> 
                    <s:property  value="#valor.id" />
                    <s:property  value="#valor.nombre" />
                    <s:property  value="#valor.correo" />
                </li>
            </s:iterator>
        </ul>
    </body>
</html>
