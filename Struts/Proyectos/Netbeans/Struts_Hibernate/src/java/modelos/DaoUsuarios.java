
package modelos;

import java.util.ArrayList;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;


public class DaoUsuarios {
   
    public ArrayList<Usuarios> getUsuarios(){
        Transaction tx=null;
        Session session=null;
        try {
            SessionFactory sf=HibernateUtil.getSessionFactory();
            session=sf.openSession();
            tx=session.beginTransaction();
            ArrayList<Usuarios> usuarios = (ArrayList<Usuarios>) session.createQuery("from Usuarios").list();
            tx.commit();
            return usuarios;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            tx.rollback();
            return null;
        }finally{
            session.close();
        }
        
     }
    
    
    }
