
package controllers;

import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import modelos.DaoUsuarios;
import modelos.Usuarios;




public class HomeController extends ActionSupport{
    
    private ArrayList<Usuarios> datos; 
    private DaoUsuarios daoUser;

    public ArrayList<Usuarios> getDatos() {
        return datos;
    }

    public DaoUsuarios getDaoUser() {
        return daoUser;
    }
    
    
    @Override
    public String execute() throws Exception {
        
        this.datos = new ArrayList<Usuarios>();
        this.daoUser = new DaoUsuarios();
        this.datos = daoUser.getUsuarios();
        
        return SUCCESS;
    }
    

 
}
