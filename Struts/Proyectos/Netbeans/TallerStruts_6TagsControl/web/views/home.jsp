<%@taglib  prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tags de control</title>
    </head>
    <body>
        <h1>Home</h1>
        
        <s:set var="texto" value="'Determinado texto en la variable'" /> <!--creamos variable-->
        <s:property value="texto" />  <!--mostrmos variable-->
        <br>
        <hr>
        
        <s:property value="num" />  <!--variable del controlador-->
        
        <br>
        
        <s:if test="%{num==121}">
            <s:property value="num" />
        </s:if>
        <s:else>
            <s:property value="'El valor de num no es igual'" />
        </s:else>
        
        <hr>
        
        <p>Recorremos un array del controlador con iterator</p>
        
        <ul>
            <s:iterator value="arreglo"  var="valor" status="estado">
                <li> 
                    <s:property  value="valor" /> 
                </li>
            </s:iterator>
        </ul>
        
        <br>
        <p>Iteramos un array de objetos de usuarios</p>
          <ul>
            <s:iterator value="arreglo2"  var="valor" status="estado">
                <li> 
                    <s:property  value="#estado.index" /> 
                    <s:property  value="#valor.nombre" />
                    <s:property  value="#valor.correo" />
                    <s:property  value="#valor.edad" />
                </li>
            </s:iterator>
        </ul>
        
    </body>
</html>
