
package controllers;

import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import modelos.Usuario;


public class HomeController extends ActionSupport{

    private int num;
    private ArrayList<String> arreglo=new ArrayList<String>(); 
    private ArrayList<Usuario> arreglo2;

    
    //getter para poder mostrar en la vista
    public ArrayList<Usuario> getArreglo2() {
        return arreglo2;
    }
    
    
    public ArrayList<String> getArreglo() {
        return arreglo;
    }
    
    public int getNum() {
        return num;
    }
    
    
    
    
    @Override
    public String execute() throws Exception {
        num=12;
        
        arreglo.add("Renault");
        arreglo.add("Ford");
        arreglo.add("Peugeot");
        
        arreglo2=new ArrayList<Usuario>();
        arreglo2.add(new Usuario("jose", "ddd@dd", 33));
        arreglo2.add(new Usuario("pepe", "zzdd@dd", 34));
        arreglo2.add(new Usuario("pepeMaria", "zzdd@dd.com", 24));
        return SUCCESS;
    }
    
    
    
}
