
<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nosotros</title>
    </head>
    <body>
        <h1>Action nosotros</h1>
       
        <ul>
            <li> <s:a action="home">Volver al home</s:a></li>
            <li> <s:a action="contacto"> Contacto</s:a></li>
            <li> <s:a href="http://google.com"> Google</s:a></li>
        </ul>
    </body>
</html>
