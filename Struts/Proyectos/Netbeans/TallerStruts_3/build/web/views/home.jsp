
<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
    </head>
    <body>
        <h1>Action home</h1>
        <p>Recuperar los datos asi, no recomendado</p>
        <p>Nombre: ${nombre}</p>
        <p>Correo: ${correo}</p> <br>
        
        <p>Recuperar los datos usadno tags lib de Struts2</p>
        <p> Nombre: <s:property value="nombre"/></p>
        <p> Apellido: <s:property value="correo"/></p>  <br>
        
        <ol>
            <li> <s:a action="nosotros">Nosostros</s:a></li>
            <li> <s:a action="contacto"> Contacto</s:a></li>
            <li> <s:a href="http://google.com"> Google</s:a></li>
        </ol>
    </body>
</html>
