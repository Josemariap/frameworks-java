<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Object Graph Navigation Language</h1>
        <ul>
            <li> Nombre: <s:property value="nombre"/> </li>
            <li> Correo: <s:property value="correo"/> </li>
            <li> Telefono: <s:property value="telefono"/> </li>
            <li> Edad: <s:property value="edad"/> </li>
        </ul>
    </body>
</html>
