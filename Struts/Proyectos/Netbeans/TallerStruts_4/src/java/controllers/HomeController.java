
package controllers;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.util.ValueStack;
import ongl.Persona;


public class HomeController extends ActionSupport {

    @Override
    public String execute() throws Exception {
        ValueStack stack=ActionContext.getContext().getValueStack();//Implementamos OGNL, podemos acceder de forma mas limpia a recursos de nuestro proyecto
        Persona p=new Persona();
        p.setNombre("Homero Simpsons");
        p.setCorreo("hhh@hhhh.com");
        p.setTelefono("46456667");
        p.setEdad(38);
        stack.push(p);//Agrego objeto al stack
        return SUCCESS;
    }
    
    public String ongl() throws Exception{
        
        return SUCCESS;
    }
    
    
    
}
