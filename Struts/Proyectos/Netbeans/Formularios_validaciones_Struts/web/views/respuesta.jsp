
<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Respuesta</title>
    </head>
    <body>
        <h1>Datos ingresados en el formulario</h1>
        
        <ul>
            <li><s:property value="nombre" /> </li>
            <li><s:property value="correo" /> </li>
            
        </ul>
    </body>
</html>
