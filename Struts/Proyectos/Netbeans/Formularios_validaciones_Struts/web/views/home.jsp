<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    </head>
    <body>
        <h1>Formularios y validaciones con STRUTS 2</h1>
        <s:fielderror/>   <!--pata visualizar los mensajes de error de las validaciones en el controlador-->
        <s:form action="respuesta">
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <s:textfield name="nombre" />
            </div>
            
            <div class="form-group">
                <label for="correo">E-mail</label>
                <s:textfield name="correo" />
            </div>
            
                <s:submit value="Enviar" title="Enviar" cssClass="btn btn-default" />
        </s:form>
    </body>
</html>
