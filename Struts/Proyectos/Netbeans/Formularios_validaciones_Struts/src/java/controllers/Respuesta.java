
package controllers;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.EmailValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.StringLengthFieldValidator;
import modelos.Usuario;


public class Respuesta extends ActionSupport{
    
    private String nombre, correo; //Se coloca el mismo nombre de variable que de los name de los input del formulario para ser 
    private Usuario user;
    //Se cargan las variables con lo ingresado del formulario a traves de los setters
    //Las validaciones del formulario tambien se realizan en el servidor, usamos los setter.(xml o annotations)
    public String getNombre() {
        return nombre;
    }
    
    //Validaciones 
    @RequiredStringValidator(message="El nombre es campo obligatorio")
    @StringLengthFieldValidator(minLength ="4", maxLength ="12", message = "El tamaño del nombre no es el adecuado")
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
    public String getCorreo() {
        return correo;
    }
    
    @RequiredStringValidator(message="El email es campo obligatorio")
    @EmailValidator(message = "El formato del email no es el adecuado")
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    
    
    
    
    @Override
    public String execute() throws Exception {
        user=new Usuario(); //Objeto que sera persistido
        user.setNombre(this.nombre);
        user.setCorreo(this.correo);
        //En la vista puedo acceder a la informacion del formulario sin crear el objeto.
        return SUCCESS;
    }
    
    
}
