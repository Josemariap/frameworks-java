/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javarevolutions.jdbc.dao;

import com.javarevolutions.jhs.persistence.Alumnos;
import com.javarevolutions.jsf.bean.BeanAlumno;
import java.util.List;


public interface IfaceAlumno {
    void insert(Alumnos obj);
    void delete(Alumnos obj);
    void update(Alumnos obj);
    List<Alumnos> getAll();
    List<Alumnos> getAllByFechas(BeanAlumno bean);
    List<Alumnos> getAllByNombre(BeanAlumno bean);
    List<Alumnos> buscarByCriterio(BeanAlumno bean);
}
