/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javarevolutions.jdbc.dao;

import com.javarevolutions.jhs.persistence.Articulos;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;


public interface IfaceArticulo {
    void insert(Articulos obj);
    void delete(Articulos obj);
    void update(Articulos obj);
    List<Articulos> getAll();
}
