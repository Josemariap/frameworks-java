
package com.javarevolutions.jdbc.dao;

import com.javarevolutions.jhs.persistence.Articulos;
import java.util.List;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;



public class ImplArticulo extends HibernateDaoSupport implements IfaceArticulo {

    @Override
    @Transactional
    public void insert(Articulos obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    @Transactional
    public void delete(Articulos obj) {
        getHibernateTemplate().delete(obj);
    }

    @Override
    @Transactional
    public void update(Articulos obj) {
        getHibernateTemplate().merge(obj);
    }

    @Override
    @Transactional
    public List getAll() {
        return getHibernateTemplate().find("from Articulos");
    }
}
