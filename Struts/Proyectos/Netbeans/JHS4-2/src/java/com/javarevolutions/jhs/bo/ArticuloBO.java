/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javarevolutions.jhs.bo;

import com.javarevolutions.jsf.bean.BeanArticulo;
import java.util.List;


public interface ArticuloBO {
    void insert(BeanArticulo obj);
    void delete(BeanArticulo obj);
    void update(BeanArticulo obj);
    List<BeanArticulo> getAll();
}
