package com.test.example.gateway;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class App {
	public static void main(String[] args) {
		System.out.println("Request en Api gateway TYK");
		ThreadRequesApiGateway hilo1 = new ThreadRequesApiGateway();
		ThreadRequesApiGateway hilo2 = new ThreadRequesApiGateway();
		ThreadRequesApiGateway hilo3 = new ThreadRequesApiGateway();

		hilo1.start();
		hilo2.start();
		hilo3.start();

	}

}
