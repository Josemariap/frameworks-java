package com.test.example.gateway;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ThreadRequesApiGateway extends Thread {
    Boolean error = false;
	public void run() {
		requestApiGatewayTyk();
	}
	
	public Boolean requestApiGatewayTyk(){
		OkHttpClient client = new OkHttpClient();
		Request request = new Request.Builder().url("https://solo-pruebas.cloud.tyk.io/meli/").get()
				.addHeader("authorization", "59e63ae4426840000122f139d10b59c74f6c4e9a4b31b818f5eb5bf2")
				.addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "2eb022a9-68c7-77cd-b6a6-015002bc71fe").build();

		for (int i = 0; i < 10; i++) {
			try {
				Response response = client.newCall(request).execute();
				System.out.println("******************request: " + i + " OK / ");
				System.out.println(response.body().string());
			} catch (IOException e) {
				e.printStackTrace();
				error = true;
			}
		} 
		return error;
		
	}

}
