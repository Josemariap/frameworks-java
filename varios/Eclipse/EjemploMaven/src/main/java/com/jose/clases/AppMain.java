package com.jose.clases;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class AppMain {

	public static void main(String[] args) {
	  
		AbstractApplicationContext contenedor=new ClassPathXmlApplicationContext("spring.xml");
		contenedor.registerShutdownHook();//Gestionar la ejecucion de spring y los beans
		Triangle triangulo=(Triangle)contenedor.getBean("triangle2");
		
		triangulo.draw();

	}

}
