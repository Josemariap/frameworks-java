import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class Ejemplo1Test {
    
	Ejemplo1 a;
	
	@Before
	public void before(){
      System.out.println("Esto se ejecuta antes de cualquier test");
	  a=new Ejemplo1();
	}
	
	@After
	public void after(){
		System.out.println("Esto se ejecuta despues de los test");
	}
	
	
	@Test()
	public void testSuma() {
	   int res=a.sumar(2, 4);
	   int esp=6;
	   assertEquals(esp, res);
	}
	
	@Test
	public void testResta(){
		 int res=a.restar(6, 1);
		 int esp=5;   
		 assertEquals(esp, res);
	}
	
	@Test(expected=ArithmeticException.class)//Espera la exception de ese tipo
	public void testDividir(){
		a.dividir(5, 0);   //Si lanza la exception la prueba estar� bien
	}
	
	
	@Test(timeout = 100)//Si el m�todo a testear tarda m�s de 100 ms dar� error el test
	public void metodoMuyLargo(){
		for(int i=0; i<100000; i++);				
	}

}
