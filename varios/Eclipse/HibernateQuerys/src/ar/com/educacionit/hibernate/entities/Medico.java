package ar.com.educacionit.hibernate.entities;

import java.lang.invoke.SwitchPoint;






public class Medico {

	private long id;
	private String nombre;
	private String nombre2;
	private String apellido;
	private int edad;
	private int matricula;
	private float sueldo;
	
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + matricula;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Medico other = (Medico) obj;
		if (matricula != other.matricula)
			return false;
		return true;
	}

	public Medico(String nombre, String nombre2, String apellido, int edad,
			int matricula, float sueldo) {
		super();
		this.nombre = nombre;
		this.nombre2 = nombre2;
		this.apellido = apellido;
		this.edad = edad;
		this.matricula = matricula;
		this.sueldo = sueldo;
	}

	@Override
	public String toString() {
		return "Medico [id=" + id + ", nombre=" + nombre + ", nombre2="
				+ nombre2 + ", apellido=" + apellido + ", edad=" + edad
				+ ", matricula=" + matricula + ", sueldo=" + sueldo + "]";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre2() {
		return nombre2;
	}

	public void setNombre2(String nombre2) {
		this.nombre2 = nombre2;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public int getMatricula() {
		return matricula;
	}

	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}

	public float getSueldo() {
		return sueldo;
	}

	public void setSueldo(float sueldo) {
		this.sueldo = sueldo;
		
     
}
	
	
 
}
