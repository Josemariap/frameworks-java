package zendesk_instagram;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Conexion {
	Connection con;
	Statement statement;
	ResultSet resultset;
	PreparedStatement ps;

	public Conexion() {
		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/instagram", "root", "");
			statement = con.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public Boolean geBCommentByIdInstagram(Long id) {
		Boolean exists = false;
		try {
			resultset = statement.executeQuery("select * from comments where id_comment_instagram =" + id);
			while (resultset.next()) {
				exists = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return exists;
	}

	public Boolean insertNewComments(Long idCommentInstagram, String fullNameInstagram) {
		Boolean insertOk = false;
		try {
			ps = con.prepareStatement("insert into comments (id_comment_instagram, full_name) values (?, ?)");
			ps.setLong(1, idCommentInstagram);
			ps.setString(2, fullNameInstagram);
			ps.executeUpdate();
			insertOk = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return insertOk;
	}
}
