package zendesk_instagram;

import java.util.Timer;
import java.util.TimerTask;

import org.zendesk.client.v2.Zendesk;
import org.zendesk.client.v2.model.Comment;
import org.zendesk.client.v2.model.Ticket;
import org.zendesk.client.v2.model.Ticket.Requester;
import org.zendesk.client.v2.model.User;

public class App {
	CommentsInstagram cInstagram = new CommentsInstagram();
	Timer myTimer = new Timer();
	TimerTask task = new TimerTask() {
		@Override
		public void run() {
			cInstagram.getComments();
		}
	};

	public void start() {
		myTimer.scheduleAtFixedRate(task, 1000, 60000);
	}

	public static void main(String[] args) {
		App app = new App();
		app.start();
	}
}
