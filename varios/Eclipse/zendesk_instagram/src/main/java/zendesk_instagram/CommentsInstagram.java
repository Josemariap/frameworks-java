package zendesk_instagram;

import java.io.IOException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CommentsInstagram {
	Conexion conexion = new Conexion();
	ZendeskClient zc = new ZendeskClient();

	public void getComments() {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInstagram = "";
		OkHttpClient client = new OkHttpClient();
		Response responseInstagram = null;
		try {
			Request requestInstagram = new Request.Builder()
					.url("https://api.instagram.com/v1/media/1654552469060902399_6553812740/comments?access_token=6553812740.9e596c8.5387e75f7cd54ca292be203f332c0154")
					.get().build();
			responseInstagram = client.newCall(requestInstagram).execute();
			jsonInstagram = responseInstagram.body().string();
			JsonNode node = mapper.readValue(jsonInstagram, JsonNode.class);
			JsonNode dataArray = node.get("data"); // array data
			for (JsonNode commentJson : dataArray) {
				publish(commentJson);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void publish(JsonNode commentJson)throws Exception {
		JsonNode userJsonComment = commentJson.get("from");
		String fullName = userJsonComment.get("full_name").toString();
		String userName = userJsonComment.get("username").toString();
		Long idCommentInstagram = commentJson.get("id").asLong();
		String textCommentInstagram = commentJson.get("text").toString();
		if (!conexion.geBCommentByIdInstagram(idCommentInstagram)) {
			conexion.insertNewComments(idCommentInstagram, fullName);
			String subject = "Comentario de instagram";
			Long requesterId = 22263639687l;
			zc.newTicket(requesterId, subject, textCommentInstagram);
			System.out.println("New instagram comment and create ticket Zendesk");
		}
	}
}
