package com.practica.jose;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

//Interfaces para cuando el context cree y destruya el bean, el contenedor llamar� a los metodos correspondientes
public class Triangle implements InitializingBean,  DisposableBean { 
	
	private List<Point>points;
	

	private Point pointA;
	private Point pointB;
	private Point pointC;
	private ApplicationContext context=null;
	
	
	public Point getPointA() {
		return pointA;
	}

   
  
	public void setPointA(Point pointA) {
		this.pointA = pointA;
	}



	public Point getPointB() {
		return pointB;
	}


	 
	public void setPointB(Point pointB) {
		this.pointB = pointB;
	}



	public Point getPointC() {
		return pointC;
	}


	
	public void setPointC(Point pointC) {
		this.pointC = pointC;
	}



	public List<Point> getPoints() {
		return points;
	}


	
	public void setPoints(List<Point> points) {
		this.points = points;
	}



	public void draw(){
		
	}


    //metodo que llamar� el contenedor cuando cree el bean
	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("Inico del bean");
		
	}


	//metodo que llamar� el contenedor cuando destruya el bean
	@Override
	public void destroy() throws Exception {
	   System.out.println("Destruccion del bean");
		
	}


	
	public void inicioBean(){
		 System.out.println("Inicio del bean");
	}

	
	public void destroyBean(){
		 System.out.println("Destroy bean");
	}
	
	
	
	
	
 /*Parte 3 
	List<Point> points;

	 
	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}
	
	 
	public void draw(){
		for(Point point:points){
			System.out.println("El triangulo tiene 3 puntos: \n " + point.getX() + " , " + point.getY());
		}
	}
*/	 
	
	
	 
/*Parte 2
	Point pointA;
	Point pointB;
	Point pointC;
	
	
	
	public Point getPointA() {
		return pointA;
	}



	public void setPointA(Point pointA) {
		this.pointA = pointA;
	}



	public Point getPointB() {
		return pointB;
	}



	public void setPointB(Point pointB) {
		this.pointB = pointB;
	}



	public Point getPointC() {
		return pointC;
	}



	public void setPointC(Point pointC) {
		this.pointC = pointC;
	}



	public void draw(){
		  System.out.println("El triangulo tiene 3 puntos: \n " + getPointA().getX() + " , " + getPointA().getY() + 
				  " \n " + getPointB().getX() + " , " + getPointB().getY() +
				  " \n " + getPointC().getX() + " , " + getPointC().getY()  );
	 }
	
	
*/	
	
	
	
 
/*Parte 1
	private String type;
	private int height;
	
	
	
	 public Triangle(String type){
		 this.type=type;
	 }
	 
	 public Triangle(int height){
		 this.height=height;
	 }
	 
	 public Triangle(String type, int height){
		 this.type=type;
		 this.height=height;
	 }
	  
	 
	 
	 public int getHeight() {
		return height;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}



	
	public void draw(){
		 
	 }
	 
   */
}
