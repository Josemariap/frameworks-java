package com.practica.jose;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//Sin utilizar xml, Spring usar� esta clase para generar los beans(basado en java)
@Configuration
public class BasadoEnJava {
   
	
	@Bean
	public Triangle triangulo1(){
		return new Triangle();
	}
}
