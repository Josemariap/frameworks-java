package com.practica.jose;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class EjemploBeanPostProcessor implements BeanPostProcessor {

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName)
			throws BeansException {
		    System.out.println("Metodo para realizar algo despues de inicilizacion BeanPostProcessor"  + beanName);
		return bean;
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName)
			throws BeansException {
		    System.out.println("Metodo para realizar algo antes de inicilizacion BeanPostProcessor"  + beanName);
		return bean;
	}
  
	
	
}
