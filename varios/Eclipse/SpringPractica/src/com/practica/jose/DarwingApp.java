package com.practica.jose;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;

public class DarwingApp {
   
	public static void main(String [] args){
		
		
      
	  //BeanFactory factory=new XmlBeanFactory(new FileSystemResource("spring.xml"));
		
		
		
	   //Basado en Java para crear los beans	
      //ApplicationContext contenedor=new AnnotationConfigApplicationContext(BasadoEnJava.class);
		
	 
	
		
	    AbstractApplicationContext contenedor=new ClassPathXmlApplicationContext("spring.xml");
		contenedor.registerShutdownHook();//Gestionar la ejecucion de spring y los beans
		Triangle triangulo=(Triangle)contenedor.getBean("triangle2");
		
		triangulo.draw();
		
	
	}
}
