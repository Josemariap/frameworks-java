package com.test.example.Timer;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class App 
{
	  ObjectMapper mapper = new ObjectMapper();
	  int cont = 0;
	  Timer myTimer = new Timer();
	  TimerTask task = new TimerTask(){
		@Override
		public void run() {
			String jsonFake = "";
			String jsonInstagram = "";
			OkHttpClient client = new OkHttpClient();

			Request requestFake = new Request.Builder()
			  .url("https://jsonplaceholder.typicode.com/posts/1")
			  .get()
			  .build();
			
			Request requestInstagram = new Request.Builder()
					  .url("https://api.instagram.com/v1/media/1654552469060902399_6553812740/comments?access_token=6553812740.9e596c8.5387e75f7cd54ca292be203f332c0154")
					  .get()
					  .build();

			Response responseFake = null;
			Response responseInstagram = null;
			try {
				responseFake = client.newCall(requestFake).execute();
				responseInstagram = client.newCall(requestInstagram).execute();
				jsonFake =  responseFake.body().string();
				jsonInstagram =  responseInstagram.body().string();
				System.out.println(jsonFake);
				System.out.println(jsonInstagram);
				Comment comment =  mapper.readValue(jsonFake, Comment.class);   
				JsonNode node = mapper.readValue (jsonInstagram, JsonNode.class);  //arbol json 
				System.out.println(comment.getBody());
				
				//lectura de la estructura json en JsonNode class que contiene el response de api instagram mapeado a clase java
				JsonNode dataArray = node.get("data"); //extraigo el array data
				System.out.println(dataArray);
				for(JsonNode commentJson : dataArray){
					JsonNode userJsonComment = commentJson.get("from");
					System.out.println(commentJson.get("text") + " de " + userJsonComment.get("full_name"));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		   
		}	
      };
	
	public void start(){
		myTimer.scheduleAtFixedRate(task, 1000, 5000);
	}
	
    public static void main( String[] args )
    {
        App app = new App();
        app.start();
           
        }
    }

