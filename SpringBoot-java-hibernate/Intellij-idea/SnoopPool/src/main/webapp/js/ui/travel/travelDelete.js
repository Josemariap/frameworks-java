snoopooling.ui.travelDelete = (function () {

    var urlBase = snoopooling.urlBase();

    var $divErrorServidor = $("#errorEliminarServidor");

    var $spinner = $("#loadingSpinnerDeleteTravel");


    function init() {
        bindearEventosABotones();
    }

    function bindearEventosABotones() {
        $("#botonEliminarViaje").on("click", function () {
            $("#errorEliminarServidor").addClass("hide");
            $spinner.removeClass("hide");
            var travelId = document.getElementById('botonEliminarViaje').value;
            eliminarViaje(travelId)
        })
    }

    function eliminarViaje(travelId) {
        snoopooling.service.travel.eliminar({"id": travelId}).done(function () {
            redireccionarAUrlRetorno()
        }).fail(function (error) {
            mostrarMensajeErrorServidor(error.responseJSON.message)
        })
    }

    function redireccionarAUrlRetorno() {
        $spinner.removeClass("hide");
        window.location.href = urlBase;
    }

    function mostrarMensajeErrorServidor(error) {
        $divErrorServidor.removeClass("hide");
        $spinner.removeClass("hide");
        document.getElementById('errorEliminarServidor').innerHTML = error;
    }

    return {
        init: init
    };

})();

$(document).on("ready", function () {
    snoopooling.ui.travelDelete.init();

    $('#driverModal').on('hidden.bs.modal', function () {
        $("#errorEliminarServidor").addClass("hide");
        $("loadingSpinnerDeleteTravel").addClass("hide");
    })
});