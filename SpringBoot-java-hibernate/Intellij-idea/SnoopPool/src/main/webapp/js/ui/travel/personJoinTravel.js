snoopooling.ui.personJoinTravel = (function () {

    var urlTravel = snoopooling.urlBase() + snoopooling.service.uriTravel();

    var $divErrorServidor = $("#errorSumarseViajeServidor");

    var $buttonJoinTravel =  $("#botonSumarseViaje");

    var $spinner = $("#loadingSpinnerJoinTravel");

    function init() {
        bindearEventosABotones();
    }

    function bindearEventosABotones() {
        $buttonJoinTravel.on("click", function () {
            $divErrorServidor.addClass("hide");
            $buttonJoinTravel.addClass("disabled");
            $spinner.removeClass("hide");
            var travelId = document.getElementById('botonSumarseViaje').value;
            SumarseViaje(travelId)
        })
    }

    function SumarseViaje(travelId) {

        snoopooling.service.travel.ingresarViaje({"id": travelId}).done(function () {
            redireccionarAUrlRetorno(travelId)
        }).fail(function (error) {
            mostrarMensajeErrorServidor(error.responseJSON.message)
        })
    }

    function redireccionarAUrlRetorno(id) {
        $spinner.addClass("hide");
        $buttonJoinTravel.removeClass("disabled");
        window.location.href = urlTravel + id;
    }

    function mostrarMensajeErrorServidor(error) {
        $divErrorServidor.removeClass("hide");
        $buttonJoinTravel.removeClass("disabled");
        $spinner.addClass("hide");
        document.getElementById('errorSumarseViajeServidor').innerHTML = error;
    }

    return {
        init: init
    };

})();

$(document).on("ready", function () {
    snoopooling.ui.personJoinTravel.init();
    $('#modalUSer').on('hidden.bs.modal', function () {
        $("#errorSumarseViajeServidor").addClass("hide");
        $("#loadingSpinnerJoinTravel").addClass("hide");
    })
});