snoopooling.ui.travel= (function () {

    var urlTravel = snoopooling.urlBase() + snoopooling.service.uriTravel();

    var $spinnerURL = $("#loadingSpinnerEditURLTravel");

    function init() {
        bindearEventoBotonEditarURL()
    }

    function bindearEventoBotonEditarURL() {
        $("#botonURLEditarViaje").on("click", function () {
            $spinnerURL.removeClass("hide");
            var travelId = document.getElementById('botonURLEditarViaje').value;
            window.location.href = urlTravel + "edit/" + travelId;
        })
    }

    return{
        init:init
    }
})();

$(document).on("ready", function () {
    snoopooling.ui.travel.init();

    $('#driverModal').on('hidden.bs.modal', function () {
        $("#loadingSpinnerEditURLTravel").addClass("hide");
    })
});