snoopooling.ui.travelNew = (function () {

    var urlTravel = snoopooling.urlBase() + snoopooling.service.uriTravel();
    var $divFechaErronea = $("#fechaErronea");
    var $divErrorServidor = $("#errorServidor");

    function init() {
        bindearEventosABotones();
    }

    function bindearEventosABotones() {
        $("#botonCrearViaje").on("click", function () {
            $("#fechaErronea").addClass("hide");

            var date = $("#date").val();
            var dateArray = date.split("T");
            var origin = "Calle 48 nro 804";
            var coorOrigin = "-34.917445,-57.9575959";
            var destination = "Sarmiento 1230";
            var coorDestination = "-34.605187,-58.3862633";
            var places = $("#places").val();

            if (moment(dateArray[0], 'yyyy-MM-dd').isValid() && moment(dateArray[1], "HH:mm").isValid()) {

                if ($("#origin-destination").val() === "Buenos Aires - La plata") {
                    origin = "Sarmiento 1230";
                    coorOrigin = "-34.605187,-58.3862633";
                    destination = "Calle 48 nro 804";
                    coorDestination = "-34.917445,-57.9575959";
                }
                insertar(origin, coorOrigin, destination, coorDestination, date, places);
            } else {
                mostrarMensajeFechaErronea()
            }

        });
    }

    function insertar(origin, coorOrigin, destination, coorDestination, date, places) {

        var travel = {
            "date": date,
            "origin": origin,
            "destination": destination,
            "places": places,
            "coorOrigin": coorOrigin,
            "coorDestination": coorDestination
        };

        snoopooling.service.travel.crear(travel).done(function (response) {
            redireccionarAUrlRetorno(response.id)
        }).fail(function (error) {
            mostrarMensajeErrorServidor(error.responseJSON.message)
        })
    }

    function redireccionarAUrlRetorno(id) {
        window.location.href = urlTravel + id;
    }

    function mostrarMensajeErrorServidor(error) {
        $divErrorServidor.removeClass("hide");
        document.getElementById('errorServidor').innerHTML = error;

    }

    function mostrarMensajeFechaErronea() {
        $divFechaErronea.removeClass("hide");
    }

    return {
        init: init
    };

})();

$(document).on("ready", function () {
    snoopooling.ui.travelNew.init();
});