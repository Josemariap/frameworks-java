<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<script type="text/javascript" src="<c:url value="/"/>js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<c:url value="/"/>js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="<c:url value="/"/>js/lib/moment.js"></script>

<script type="text/javascript" src="<c:url value="/"/>js/app.js"></script>
<script type="text/javascript" src="<c:url value="/"/>js/ui/ui.js"></script>
<script type="text/javascript" src="<c:url value="/"/>js/service/service.js"></script>
<script type="text/javascript" src="<c:url value="/"/>js/service/travel/travel.js"></script>
<script type="text/javascript" src="<c:url value="/"/>js/ui/travel/travel.js"></script>
<script type="text/javascript" src="<c:url value="/"/>js/ui/travel/travelNew.js"></script>
<script type="text/javascript" src="<c:url value="/"/>js/ui/travel/travelDelete.js"></script>
<script type="text/javascript" src="<c:url value="/"/>js/ui/travel/travelEdit.js"></script>
<script type="text/javascript" src="<c:url value="/"/>js/ui/travel/personOutTravel.js"></script>
<script type="text/javascript" src="<c:url value="/"/>js/ui/travel/personJoinTravel.js"></script>
<script type="text/javascript" src="<c:url value="/"/>js/ui/travel/personDeleteTravel.js"></script>
<script type="text/javascript" src="<c:url value="/"/>js/ui/travel/travelSuggestTime.js"></script>