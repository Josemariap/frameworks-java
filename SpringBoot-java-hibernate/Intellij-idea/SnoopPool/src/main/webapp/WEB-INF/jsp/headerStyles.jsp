<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<link href="<c:url value="/"/>css/snoopooling.css" rel="stylesheet">
<link href="<c:url value="/"/>css/travel.css" rel="stylesheet">
<link href="<c:url value="/"/>css/travel-person.css" rel="stylesheet">
<link href="<c:url value="/"/>css/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="<c:url value="/"/>template/assets/css/font-awesome.min.css" rel="stylesheet">
<link href="<c:url value="/"/>css/google/font-family.css" rel="stylesheet">