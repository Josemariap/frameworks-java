<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="container">
    <div class="card">
        <div class="container-fliud">
            <div class="wrapper row">
                <div class="preview col-md-6">

                    <div class="preview-pic tab-content">
                        <div class="tab-pane active" id="pic-1">
                            <a href="https://www.google.com.ar/maps/dir/${travel.coorOrigin}/${travel.coorDestination}"
                               target="_blank">
                                <img src="https://maps.googleapis.com/maps/api/staticmap?size=600x300&maptype=roadmap&markers=color:blue%7Clabel:A%7C${travel.coorOrigin}&markers=color:red%7Clabel:B%7C${travel.coorDestination}&path=color:0xff0000ff|weight:5|${travel.coorOrigin}|${travel.coorDestination}&key=AIzaSyAUaesycE_TqlS7jcjWOuVe0tO6axkqNRM"
                                     class="img-maps"/>
                            </a>
                        </div>
                    </div>
                    <ul class="preview-thumbnail nav nav-tabs">

                        <table class="table">
                            <thead>
                            <tr>
                                <th><spring:message code="travel.origin"/><span class="color blue point-a"></span></th>
                                <th><spring:message code="travel.destination"/><span class="color red point-b"></span>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>${travel.origin}</td>
                                <td>${travel.destination}</td>
                            </tr>
                            </tbody>
                        </table>

                    </ul>

                </div>
                <div class="details col-md-6">

                    <h3 class="product-title">

                        <fmt:formatDate pattern="d" value="${travel.date}"/>
                        <fmt:formatDate pattern="MMM" dateStyle="FULL" value="${travel.date}"/>

                    </h3>


                    <h4 class="price"><spring:message code="travel.exiTime"/>: <span><fmt:formatDate pattern="H"
                                                                                                     value="${travel.date}"/>:<fmt:formatDate
                            pattern="m" value="${travel.date}"/></span></h4>

                    <div class="row">
                        <div class="span4 well">

                            <div class="row">


                                <div class="col-sm-4"><img src="${travel.person.avatar}"
                                                           class="img-circle" data-toggle="modal"
                                                           data-target="#driverModal"/>
                                </div>

                                <div class="col-sm-8">
                                    <p><spring:message code="travel.driver"/></p>
                                    <p><strong>${travel.person.name}</strong></p>
                                </div>


                                <!-- Modal -->
                                <div class="modal fade" id="driverModal" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" style="z-index:1200">
                                        <div class="modal-content box icon">
                                            <div class="modal-header modal-header-primary box icon">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-hidden="true">x
                                                </button>
                                                <img alt="" src="${travel.person.avatar}"
                                                     class="img-circle avatar-resize">
                                                <h2><i class="fa fa-user"></i> ${travel.person.name}</h2>
                                                <h4><i class="fa fa-envelope-o"></i> ${travel.person.mail}</h4>
                                                <div id="errorEliminarServidor" class="hide"></div>
                                                <div id="loadingSpinner1" class="hide">
                                                    <img src="<c:url value="/"/>image/loading-spinner.gif">
                                                </div>
                                                <c:choose>
                                                    <c:when test="${travel.person.id == user.id}">
                                                        <div class="row">
                                                            <div class="col-xs-3 col-md-3"></div>
                                                            <div class="col-xs-3 col-md-3">
                                                                <button class="btn btn-warning btn-lg" type="button"
                                                                        id="botonURLEditarViaje" value="${travel.id}"><i
                                                                        id="loadingSpinnerEditURLTravel"
                                                                        class="fa fa-spinner fa-spin hide"></i>
                                                                    <spring:message code="travel.modal.button.update"/>
                                                                </button>
                                                            </div>
                                                            <div class="col-xs-3 col-md-3">
                                                                <button class="btn btn-danger btn-lg" type="button"
                                                                        id="botonEliminarViaje" value="${travel.id}"><i
                                                                        id="loadingSpinnerDeleteTravel"
                                                                        class="fa fa-spinner fa-spin hide"></i>
                                                                    <spring:message code="travel.modal.button.delete"/>
                                                                </button>
                                                            </div>
                                                            <div class="col-xs-3 col-md-3"></div>
                                                        </div>
                                                    </c:when>
                                                </c:choose>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Modal -->

                            </div>
                        </div>
                    </div>

                    <h5 class="colors"><spring:message code="travel.persons"/>:
                        <c:forEach items="${travel.persons}" var="person">

                            <c:if test="${person.id==user.id}">
                                <c:set var="userInTravel" value="${true}"/>
                            </c:if>

                            <img src="${person.avatar}" class="img-circle" data-toggle="modal"
                                 data-target="#modalUSer"/>

                            <!-- Modal -->
                            <div class="modal fade" id="modalUSer" tabindex="-1" role="dialog">
                                aria-labelledby="myModalLabel" aria-hidden="true" value="${travel.id}">
                                <div class="modal-dialog" style="z-index:1200">
                                    <div class="modal-content box icon">
                                        <div class="modal-header modal-header-primary box icon">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                                                    id="botonCancelar">
                                                x
                                            </button>
                                            <div id="modalContext"> <!-- init modalContex -->

                                                <img alt="" src="${person.avatar}" class="img-circle avatar-resize">
                                                <h2><i class="fa fa-user"></i> ${person.name}</h2>
                                                <h4><i class="fa fa-envelope-o"></i> ${person.mail}</h4>

                                                <div id="successSuggestTime" class="alert alert-success hide">
                                                    <spring:message code="travel.modal.success.suggestTime"/>
                                                </div>

                                                <c:choose>
                                                    <c:when test="${not empty userInTravel}">
                                                        <div class="row">

                                                            <div class="col-xs-2 col-md-2"></div>

                                                            <div class="col-xs-3 col-md-3" style="padding: 0;">
                                                                <!-- init boton sugerir hora -->
                                                                <button class="btn btn-default btn-lg btn-block"
                                                                        type="button" id="botonSugerirHora"
                                                                        value="${travel.id}">
                                                                    <i id="loadingSpinnerExit"
                                                                       class="fa fa-spinner fa-spin hide"></i>
                                                                    <spring:message code="travel.modal.suggestTime"/>
                                                                </button>
                                                            </div><!-- end -->

                                                            <div class="col-xs-2 col-md-2"></div>

                                                            <div class="col-xs-3 col-md-3" style="padding: 0;">
                                                                <div id="errorSalirViajeServidor"
                                                                     class="alert alert-warning hide">
                                                                </div>
                                                                <button class="btn btn-default btn-lg btn-block"
                                                                        type="button" id="botonSalirViaje"
                                                                        value="${travel.id}">
                                                                    <i id="loadingSpinnerExit"
                                                                       class="fa fa-spinner fa-spin hide"></i>
                                                                    <spring:message code="travel.modal.outTravel"/>
                                                                </button>
                                                            </div>
                                                            <div class="col-xs-2 col-md-2"></div>
                                                        </div>
                                                    </c:when>
                                                </c:choose>

                                                <c:choose>
                                                    <c:when test="${travel.person.id==user.id}">
                                                        <div class="row">
                                                            <div class="col-xs-4 col-md-4"></div>
                                                            <div class="col-xs-4 col-md-4">
                                                                <input type="hidden" id="personId" value="${person.id}">
                                                                <input type="hidden" id="travelId" value="${travel.id}">
                                                                <button class="btn btn-danger btn-lg"
                                                                        type="button" id="botonEliminarUsuarioViaje">
                                                                    <i id="loadingSpinnerDeleteUser"
                                                                       class="fa fa-spinner fa-spin hide"></i>
                                                                    <spring:message code="travel.modal.cancelUser"/>
                                                                </button>
                                                            </div>
                                                            <div class="col-xs-4 col-md-4"></div>
                                                        </div>
                                                    </c:when>
                                                </c:choose>

                                            </div><!-- end modalContex -->

                                            <div id="suggestTime" class="hide">
                                                <h5><spring:message code="travel.modal.suggestTime.selectTime"/></h5>

                                                <div id="errorServer" class="alert alert-danger hide"></div>

                                                <form role="form" id="formSuggesTime">
                                                    <div class="row" style="margin-bottom: 10px">
                                                        <div class="col-xs-3 col-md-3"></div>
                                                        <div class="col-xs-3 col-md-3" style="padding-right: 1px">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i
                                                                        class="fa fa-clock-o"></i></span>
                                                                <input type="number" id="numberHour" min="0" max="23"
                                                                       class="form-control input-sm" placeholder="<spring:message code="travel.modal.suggestTime.hour"/>">
                                                            <label class="input-group-addon">:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3 col-md-3" style="padding-left: 1px">
                                                            <div class="input-group">
                                                                <input type="number" id="numberMinutes" min="0" max="59"
                                                                       class="form-control input-sm" placeholder="<spring:message code="travel.modal.suggestTime.minutes"/>">
                                                                <span class="input-group-addon"><i
                                                                        >hs</i></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3 col-md-3"></div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-3 col-md-3"></div>
                                                        <div class="col-xs-6 col-md-6">
                                                         <textarea class="form-control" id="textArea" rows="3"
                                                                   id="comments" style="margin-bottom: 15px"
                                                                   placeholder="<spring:message code="travel.modal.textArea.placeholder"/>"></textarea>
                                                        </div>
                                                        <div class="col-xs-3 col-md-3"></div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-1 col-md-1"></div>
                                                        <div class="col-xs-3 col-md-3">
                                                            <button class="btn btn-default btn-lg btn-lg btn-block"
                                                                    type="button" id="botonConfirmarHoraSugerida"
                                                                    value="${travel.id}">
                                                                <i id="loadingSpinnerSuggestTime"
                                                                   class="fa fa-spinner fa-spin hide"></i>
                                                                <spring:message
                                                                        code="travel.modal.suggestTime.button.Confirm"/>
                                                            </button>
                                                        </div>
                                                        <div class="col-xs-4 col-md-4"></div>
                                                        <div class="col-xs-3 col-md-3">
                                                            <button class="btn btn-default btn-lg btn-block "
                                                                    type="button" id="botonVolver" value="${travel.id}">
                                                                <i id="loadingSpinnerSuggestTime"
                                                                   class="fa fa-spinner fa-spin hide"></i>
                                                                <spring:message
                                                                        code="travel.modal.suggestTime.button.Back"/>
                                                            </button>
                                                        </div>
                                                        <div class="col-xs-1 col-md-1"></div>
                                                    </div>
                                                </form>
                                                <div id="errorValidacion" class="alert alert-danger hide">
                                                    <spring:message code="travel.modal.validation.error"/>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal -->


                        </c:forEach>

                        <c:forEach begin="${fn:length(travel.persons)}" end="${travel.places - 1}"
                                   varStatus="loop">
                            <c:set var="userInTravel" value="${false}"/>
                            <c:forEach items="${travel.persons}" var="person">
                                <c:if test="${person.id==user.id}">
                                    <c:set var="userInTravel" value="${true}"/>
                                </c:if>
                            </c:forEach>
                            <c:choose>
                                <c:when test="${userInTravel}">
                                    <img src="<c:url value="/"/>image/empty-avatar.gif" class="img-circle"/>
                                </c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${travel.person.id == user.id}">
                                            <img src="<c:url value="/"/>image/empty-avatar.gif"
                                                 class="img-circle"/>
                                        </c:when>
                                        <c:otherwise>
                                            <img src="<c:url value="/"/>image/empty-avatar.gif"
                                                 class="img-circle" data-toggle="modal"
                                                 data-target="#addTravelModal"/>


                                            <!-- Modal -->
                                            <div class="modal fade" id="addTravelModal"
                                                 tabindex="-1" role="dialog"
                                                 aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" style="z-index:1200">
                                                    <div class="modal-content box icon">
                                                        <div class="modal-header modal-header-primary box icon">
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-hidden="true">x
                                                            </button>
                                                            <h1><i class="fa fa-handshake-o"></i>
                                                                <spring:message code="travel.modal.addTravel"/></h1>
                                                        </div>
                                                        <div class="modal-body box icon">
                                                            <img src="<c:url value="/"/>image/add-person-travel.jpg"
                                                                 class="img-resize">
                                                            <div id="errorSumarseViajeServidor"
                                                                 class="alert alert-warning hide">
                                                            </div>
                                                            <button class="btn btn-success size-position-add-travel btn-lg"
                                                                    type="button" id="botonSumarseViaje"
                                                                    value="${travel.id}">
                                                                <i id="loadingSpinnerJoinTravel"
                                                                   class="fa fa-spinner fa-spin hide"></i>
                                                                <spring:message code="travel.modal.button.addtravel"/>
                                                            </button>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->
                                            <!-- Modal -->
                                        </c:otherwise>
                                    </c:choose>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </h5>
                    <div class="row">
                        <div class="col-md-4 col-sm-4"></div>
                        <div class="col-md-4 col-sm-4">

                            <form action="<c:url value="/"/>" method="GET">
                                <button class="btn btn-default" id="back"><spring:message
                                        code="travel.button.back"/></button>
                            </form>

                        </div>
                        <div class="col-md-4 col-sm-4"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>







