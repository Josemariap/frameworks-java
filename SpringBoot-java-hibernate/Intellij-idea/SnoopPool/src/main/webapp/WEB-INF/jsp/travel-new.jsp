<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="container">
    <br>
    <br>
    <div class="row" id="main">
        <div class="col-md-4 well" id="leftPanel">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <img src="<c:url value="/"/>image/map-car.jpg" alt="Texto Alternativo"
                             class="img-circle img-thumbnail">
                        <h2><spring:message code="travel.new.title"/></h2>
                        <p><spring:message code="travel.new.description"/></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8 well" id="rightPanel">
            <div class="row">
                <div class="col-md-12">
                    <form role="form">
                        <h2><spring:message code="travel.new.header.form.1"/>
                            <small><spring:message code="travel.new.header.form.2"/></small>
                        </h2>
                        <hr class="colorgraph">

                        <div class="row">

                            <div id="fechaErronea" class="alert alert-warning hide">
                                <strong><spring:message code="travel.new.error"/></strong> <spring:message
                                    code="travel.new.error.date"/>
                            </div>
                            <div id="errorServidor" class="alert alert-warning hide">
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="date"><spring:message code="travel.new.form.date"/></label>
                                    <input type="datetime-local" name="date" id="date" class="form-control input-lg"
                                           placeholder="Fecha y Hora" tabindex="1">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="places"><spring:message code="travel.new.form.place"/></label>
                                    <select class="form-control input-lg" id="places" name="places">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="origin-destination"><spring:message code="travel.new.form.originDestination"/></label>
                                <select class="form-control input-lg" id="origin-destination" name="origin-destination">
                                    <option>Buenos Aires - La plata</option>
                                    <option>La plata - Buenos Aires</option>
                                </select>
                            </div>
                        </div>

                        <hr class="colorgraph">
                        <div class="row">
                            <div class="col-xs-12 col-md-6"></div>
                            <div class="col-xs-12 col-md-6">
                                <a id="botonCrearViaje" class="btn btn-success btn-block btn-lg"><spring:message
                                        code="travel.new.form.button"/></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



