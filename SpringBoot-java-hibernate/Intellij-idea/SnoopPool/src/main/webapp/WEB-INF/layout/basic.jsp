<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><tiles:getAsString name="title"/></title>
    <tiles:insertAttribute name="headerStyles"/>
</head>
<body>
<tiles:insertAttribute name="header"/>

<sec:authorize access="isAuthenticated()">
    <c:choose>
        <c:when test="${user==null}">
            <a><spring:message code="authentication.dontUserSnoop"/></a>
        </c:when>
        <c:otherwise>
            <tiles:insertAttribute name="body"/>
        </c:otherwise>
    </c:choose>
</sec:authorize>

<sec:authorize access="isAnonymous()">
    <a><spring:message code="authentication.isAnonymous"/></a>
</sec:authorize>

<tiles:insertAttribute name="footerJavascript"/>
</body>
</html>