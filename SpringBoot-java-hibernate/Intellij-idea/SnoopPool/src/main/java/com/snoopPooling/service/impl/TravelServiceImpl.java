package com.snoopPooling.service.impl;

import com.snoopPooling.Utils.Validator;
import com.snoopPooling.domain.Person;
import com.snoopPooling.domain.Travel;
import com.snoopPooling.repository.TravelRepository;
import com.snoopPooling.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class TravelServiceImpl implements TravelService {

    @Autowired
    TravelRepository travelRepository;

    @Autowired
    PersonService personService;

    @Autowired
    NotificationService notificationService;

    @Autowired
    MessageService messageService;

    @Autowired
    NotificationWorkplaceService notificationWorkplaceService;
    @Value("${snoopooling.url.base}")
    private String urlBase;
    @Value("${snoopooling.path.travels}")
    private String pathTravels;
    @Value("${snoopooling.path.travel.edit}")
    private String pathTravelEdit;

    @Override
    public Travel save(Travel travel, Authentication authentication) {
        Person person = (Person) personService.getUserWhitSession(authentication);
        travel.setPerson(person);
        List<Travel> travels = travelRepository.findAllByDateAndPerson(travel.getDate(), person);
        if (!travels.isEmpty()) {
            for (Travel travelByPerson : travels) {
                if (travelByPerson.getOrigin().equals(travel.getOrigin()) && travelByPerson.getDestination().equals(travel.getDestination())) {
                    throw new DataRetrievalFailureException(messageService.getMessage("save.trowException.existSimilarTravel"));
                }
            }
        }

        if (!Validator.validateTravel(travel)) {
            throw new DataRetrievalFailureException(messageService.getMessage("save.trowException.dontSaveTravel"));
        }
        travel = travelRepository.save(travel);
        String data = messageService.buildWorkPlaceBody(travel.getDate(), travel.getOrigin(), travel.getDestination(), "save");
        // String urlTravel = snoopoolingBase + snoopoolingTravel;
        // notificationWorkplaceService.sendNotificationWorkplace(urlTravel, data);
        return travel;
    }

    @Override
    public Travel update(Travel travelUpdate, Authentication authentication) {
        Travel travel = travelRepository.findOne(travelUpdate.getId());
        Person person = (Person) personService.getUserWhitSession(authentication);
        if (!travel.getPerson().getId().equals(person.getId())) {
            throw new DataRetrievalFailureException(messageService.getMessage("update.trowException.dontOwnerTravel"));
        }
        travel.setOrigin(travelUpdate.getOrigin());
        travel.setDestination(travelUpdate.getDestination());
        travel.setCoorOrigin(travelUpdate.getCoorOrigin());
        travel.setCoorDestination(travelUpdate.getCoorDestination());
        travel.setDate(travelUpdate.getDate());
        travel.setPlaces(travelUpdate.getPlaces());
        List<Travel> travels = travelRepository.findAllByDateAndPersonAndIdNot(travel.getDate(), travel.getPerson(), travel.getId());
        if (!travels.isEmpty()) {
            for (Travel travelByPerson : travels) {
                if (travelByPerson.getOrigin().equals(travel.getOrigin()) && travelByPerson.getDestination().equals(travel.getDestination())) {
                    throw new DataRetrievalFailureException(messageService.getMessage("update.trowException.ExistsSameTravel"));
                }
            }
        }

        if (!Validator.validateTravel(travel) || !Validator.validatePlaceTravel(travel.getPlaces(), travel.getPersons().size())) {
            throw new DataRetrievalFailureException(messageService.getMessage("update.throwException.validator"));
        }
        for (Person p : travel.getPersons()) {
            String msg = messageService.buildMailBody(travel.getDate(), travel.getOrigin(), travel.getDestination(), "update");
            String title = messageService.getMessage("update.email.subject");
            notificationService.sendNotificationsMail(p.getMail(), title, msg);
        }
        return travelRepository.save(travel);
    }

    @Override
    public Travel getTravelById(Long id) {
        return travelRepository.findOne(id);
    }

    @Override
    public List<Travel> getAllTravel() {
        Date date = new Date();
        return travelRepository.findAllByDateIsGreaterThanOrderByDateAsc(date);
    }


    @Override
    public Boolean delete(Long idTravel, Authentication authentication) {
        Travel travel = travelRepository.findById(idTravel);
        Person person = (Person) personService.getUserWhitSession(authentication);

        if (travel.getPerson().getId().equals(person.getId())) {
            travelRepository.delete(travel);
            List<Travel> travels = travelRepository.findAllByDateAndOriginAndDestination(travel.getDate(), travel.getOrigin(), travel.getDestination());
            String travelsUrl="";
            if(!travels.isEmpty()){
                travelsUrl = messageService.getMessage("delete.email.idemTravel") + "\n" + urlBase + pathTravels + travel.getDate() + "/" + travel.getCoorOrigin() + "&" + travel.getCoorDestination() + "/";
            }
            for (Person p : travel.getPersons()) {
                String msg = messageService.buildMailBody(travel.getDate(), travel.getOrigin(), travel.getDestination(), "delete");
                String title = messageService.getMessage("delete.email.subject");
                msg += travelsUrl;
                notificationService.sendNotificationsMail(p.getMail(), title, msg);
            }
            return true;
        } else {
            throw new DataRetrievalFailureException(messageService.getMessage("delete.throwException.dontOwnerTravel"));
        }
    }


    @Override
    public List<Travel> getAllTravelBytDate(Date date) {
        return travelRepository.findAllByDate(date);
    }

    @Override
    public List<Travel> getAllTravelByDateAndOriginAndDestination(Date date, String origin, String destination) {
        return travelRepository.findAllByDateAndOriginAndDestination(date, origin, destination);
    }

    @Override
    public List<Travel> findAllByDateAndCoorOriginAndAndCoorDestination(String date, String coorOrigin, String coorDestination) throws ParseException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date fecha = formatter.parse(date);

        return travelRepository.findAllByDateAndCoorOriginAndAndCoorDestination(fecha, coorOrigin, coorDestination);
    }

    @Override
    public List<Travel> getAllTravelByPerson(Person person) {
        return travelRepository.findAllByPerson(person);
    }

    @Override
    public Boolean cancelPersonByTravel(Long travelId, Long personId, Authentication authentication) {
        Person person = personService.getPersonById(personId);
        Person personSession = (Person) personService.getUserWhitSession(authentication);
        Travel travel = travelRepository.findOne(travelId);
        if (! travel.getPerson().getId().equals(personSession.getId())) {
            throw new DataRetrievalFailureException(messageService.getMessage("cancelPersonByTravel.throwException"));
        }
        travel.getPersons().remove(person);
        travelRepository.save(travel);
        String msg = messageService.buildMailBody(travel.getDate(), travel.getOrigin(), travel.getDestination(), "cancelPersonByTravel");
        String title = messageService.getMessage("cancelPersonByTravel.subject");
        notificationService.sendNotificationsMail(person.getMail(), title, msg);
        return true;
    }

    @Override
    public Boolean personJoinTravel(Long idTravel, Authentication authentication) {
        Person person = (Person) personService.getUserWhitSession(authentication);
        Travel travel = travelRepository.findOne(idTravel);
        List<Travel> travels = travelRepository.findAllByDateAndOriginAndDestinationAndPersonsAndIdNot(
                travel.getDate(), travel.getOrigin(), travel.getDestination(), person, travel.getId());
        if (travels.size() != 0) {
            throw new DataRetrievalFailureException(messageService.getMessage("personJoinTravel.throwException"));
        }
        if (travel.getPersons().size() > travel.getPlaces() && travel.getPersons().contains(person)) {
            throw new DataRetrievalFailureException(messageService.getMessage("personJoinTravel.contains.throwException"));
        }
        travel.getPersons().add(person);
        travelRepository.save(travel);
        String msg = messageService.buildMailBody(travel.getDate(), travel.getOrigin(), travel.getDestination(), "personJoinTravel");
        String title = messageService.getMessage("personJoinTravel.subject");
        notificationService.sendNotificationsMail(travel.getPerson().getMail(), title, msg);
        return true;
    }

    @Override
    public Boolean personOutTravel(Long idTravel, Authentication authentication) {
        Person person = (Person) personService.getUserWhitSession(authentication);
        Travel travel = travelRepository.findOne(idTravel);
        if (travel.getPersons().contains(person)) {
            travel.getPersons().remove(person);
            travelRepository.save(travel);
            String msg = messageService.buildMailBody(travel.getDate(), travel.getOrigin(), travel.getDestination(), "personOutTravel");
            String title = messageService.getMessage("personOutTravel.subject");
            notificationService.sendNotificationsMail(travel.getPerson().getMail(), title, msg);
            return true;
        } else {
            throw new DataRetrievalFailureException(messageService.getMessage("personOutTravel.throwException"));
        }
    }

    @Override
    public Boolean suggestTimeChange(Long idTravel,String time, String comment, Authentication authentication) {
        Travel travel = travelRepository.findOne(idTravel);
        Person personSession = (Person) personService.getUserWhitSession(authentication);
        if(personSession == null){
            throw new DataRetrievalFailureException(messageService.getMessage("suggestTimeChange.throwException"));
        }
        if(!travel.getPersons().contains(personSession)){
            throw new DataRetrievalFailureException(messageService.getMessage("suggestTimeChange.contains.throwException"));
        }
        String msg= messageService.buildMailBodySuggestTimeChange(travel.getDate(), travel.getOrigin(), travel.getDestination(),personSession.getName(), time, comment );
        String title= messageService.getMessage("suggestTimeChange.subject");
        String travelsUrl = messageService.getMessage("suggestTimeChange.updateTravelLink") + "\n" + urlBase + pathTravelEdit + travel.getId();
        msg += travelsUrl;
        notificationService.sendNotificationsMail(travel.getPerson().getMail(), title, msg);
        return true;
    }


}
