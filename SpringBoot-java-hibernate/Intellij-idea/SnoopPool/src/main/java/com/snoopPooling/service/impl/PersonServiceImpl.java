package com.snoopPooling.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.snoopPooling.domain.Person;
import com.snoopPooling.repository.PersonRepository;
import com.snoopPooling.repository.TravelRepository;
import com.snoopPooling.service.NotificationService;
import com.snoopPooling.service.PersonService;
import com.snoopPooling.service.TravelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    PersonRepository personRepository;

    @Override
    public Person getPersonById(Long id) {
        return personRepository.findOne(id);
    }

    @Override
    public Person getPersonByMail(String mail) {
        return personRepository.findByMail(mail);
    }

    @Override
    public Object getUserWhitSession(Authentication authentication) {

        ObjectMapper mapper = new ObjectMapper();
        try {
            String pattern = "(\"|\')email(\"|\'):(\"|\')(.*?)((\"|\'),(\"|\')email_verified(\"|\'))";
            String string = mapper.writeValueAsString(authentication);
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(string);
            if (m.find()) {
                return personRepository.findByMail(m.group(4));
            } else return null;

        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

}
