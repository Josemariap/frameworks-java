package com.snoopPooling.repository;

public interface NotificationWorkplaceRepository {

    Boolean sendNotificationWorkplace(String url, String data) throws  Exception;
}
