package com.snoopPooling.service.impl;

import com.snoopPooling.repository.NotificationRepository;
import com.snoopPooling.repository.impl.NotificationRepositoryImpl;
import com.snoopPooling.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class NotificationServiceImpl implements NotificationService {

    @Autowired
    NotificationRepository notificationRepository;

    @Override
    public Boolean sendNotificationsMail(String mail, String title, String message) {
       return  notificationRepository.sendNotificationMail(mail, title, message);
    }
}


