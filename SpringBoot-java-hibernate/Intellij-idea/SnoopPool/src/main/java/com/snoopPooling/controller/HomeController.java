package com.snoopPooling.controller;

import com.snoopPooling.domain.Person;
import com.snoopPooling.repository.TravelRepository;
import com.snoopPooling.service.PersonService;
import com.snoopPooling.service.TravelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @Autowired
    PersonService personService;

    @Autowired
    TravelService travelService;

    @RequestMapping(value = "/")
    public String home(Authentication authentication, Model model) {
        model.addAttribute("user", personService.getUserWhitSession(authentication));
        model.addAttribute("travels", travelService.getAllTravel());
        return "site.home";
    }

}
