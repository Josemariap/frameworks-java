package com.snoopPooling.Utils;

import com.snoopPooling.domain.Travel;
import org.springframework.dao.DataRetrievalFailureException;

public class Validator {

    private static  int min = 0;
    private  static  int max = 6;

    public static Boolean validateTravel(Travel travel){
        if(travel.getPerson() !=null && travel.getPlaces() > min && travel.getPlaces() <= max && travel.getOrigin() !=null
                && travel.getDestination() !=null && travel.getCoorOrigin() !=null && travel.getCoorDestination() !=null
                && travel.getDate() !=null){
            return true;
        }else{
           return false;
     }
    }

    public static Boolean validatePlaceTravel(int places, int cantPersons){
        if(places < cantPersons){
            throw new DataRetrievalFailureException("La cantidad de places no puede ser menor a las personas del viaje");
        }else{
            return true;
        }
    }

}
