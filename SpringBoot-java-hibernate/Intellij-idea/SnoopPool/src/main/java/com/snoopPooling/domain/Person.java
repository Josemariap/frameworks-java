package com.snoopPooling.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "persons")
public class Person implements Serializable {
	
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name ="ID")
  private Long id;
  
  @Column(name ="NAME")
  private String name;
  
  @Column(name ="MAIL")
  private String mail;
  
  @Column(name ="AVATAR")
  private String avatar;

  @Column(name ="ID_WORKPLACE")
  private int idWorkplace;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "persons")
  @JsonIgnore
  private Set<Travel> myTravels;

  @ManyToMany(fetch = FetchType.LAZY, mappedBy="person")
  @JsonIgnore
  private Set<Travel> travels;

	public Person() {}
	  
	public Person(String name, String mail, int idWorkplace, String avatar) {
		this.name = name;
		this.mail = mail;
		this.avatar = avatar;
		this.idWorkplace = idWorkplace;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public int getIdWorkplace() {
		return idWorkplace;
	}
	public void setIdWorkplace(int idWorkplace) {
		this.idWorkplace = idWorkplace;
	}
	public Set<Travel> getTravels() {
		return travels;
	}
	public void setTravels(Set<Travel> travels) {
		this.travels = travels;
	}
	public Set<Travel> getMyTravels() {
		return myTravels;
	}
	public void setMyTravels(Set<Travel> myTravels) {
		this.myTravels = myTravels;
	}
}
