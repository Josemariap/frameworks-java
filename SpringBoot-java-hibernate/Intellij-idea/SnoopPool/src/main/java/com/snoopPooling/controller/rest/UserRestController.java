package com.snoopPooling.controller.rest;

import com.snoopPooling.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class UserRestController {

    @Autowired
    PersonService personService;

    @RequestMapping("/api/user")
	public Object getUser(Authentication authentication) {
		return personService.getUserWhitSession(authentication);
	}

}
