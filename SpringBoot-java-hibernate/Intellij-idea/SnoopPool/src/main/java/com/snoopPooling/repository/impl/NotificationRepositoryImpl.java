package com.snoopPooling.repository.impl;

import com.snoopPooling.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Repository
public class NotificationRepositoryImpl implements NotificationRepository{

    @Value("${mail.username}")
    private String username;
    @Value("${mail.password}")
    private String password;
    @Value("${mail.port}")
    private String port;
    @Value("${mail.host}")
    private String host;
    @Value("${mail.socketfactory}")
    private String socketFactory;
    @Value("${mail.auth}")
    private String auth;

    @Override
    public Boolean sendNotificationMail(String mail, String title, String message) {
        Properties mailServerProperties;
        mailServerProperties = System.getProperties();
        mailServerProperties.put("mail.smtp.host", host);
        mailServerProperties.put("mail.smtp.socketFactory.port", port);
        mailServerProperties.put("mail.smtp.socketFactory.class", socketFactory);
        mailServerProperties.put("mail.smtp.auth", auth);
        mailServerProperties.put("mail.smtp.port", port);

        try {
            javax.mail.Session getMailSession = Session.getDefaultInstance(
                    mailServerProperties,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                        }
                    });
            javax.mail.internet.MimeMessage generateMailMessage;
            generateMailMessage = new MimeMessage(getMailSession);
            generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(mail));
            generateMailMessage.setSubject(title);
            String msg = "<h1>Aviso de la app SnoopPooling</h1>";
            msg += message;
            generateMailMessage.setContent(msg, "text/html");
            Transport.send(generateMailMessage);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
