package com.snoopPooling.service;

import com.snoopPooling.domain.Person;
import org.springframework.security.core.Authentication;

public interface PersonService {

    Person getPersonById(Long id);

    Person getPersonByMail(String mail);

    Object getUserWhitSession(Authentication authentication);

}

