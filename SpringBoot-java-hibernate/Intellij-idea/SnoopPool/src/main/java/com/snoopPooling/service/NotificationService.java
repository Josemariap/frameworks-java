package com.snoopPooling.service;

import com.snoopPooling.domain.Person;

public interface NotificationService {

    Boolean sendNotificationsMail(String mail, String titulo, String mensaje);
}

