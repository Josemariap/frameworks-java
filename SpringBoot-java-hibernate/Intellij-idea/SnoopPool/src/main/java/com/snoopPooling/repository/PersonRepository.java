package com.snoopPooling.repository;

import com.snoopPooling.domain.Person;
import org.springframework.data.repository.CrudRepository;



public interface PersonRepository extends CrudRepository<Person, Long> {
    Person findByMail(String mail);

}
