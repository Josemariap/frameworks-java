package com.snoopPooling.controller.rest;

import com.snoopPooling.domain.Travel;
import com.snoopPooling.service.TravelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
public class TravelRestController {

    @Autowired
    private TravelService travelService;

    @RequestMapping(value ="/api/travel", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Travel create(HttpServletRequest request, Authentication authentication) throws ParseException {
        Travel travel = new Travel();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date date = formatter.parse(request.getParameter("date"));
        travel.setDate(date);
        travel.setCoorDestination(request.getParameter("coorDestination"));
        travel.setCoorOrigin(request.getParameter("coorOrigin"));
        travel.setOrigin(request.getParameter("origin"));
        travel.setDestination(request.getParameter("destination"));
        travel.setPlaces(Integer.parseInt(request.getParameter("places")));
        return travelService.save(travel, authentication);
    }

    @RequestMapping(value ="/api/travel/{id}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Travel update(HttpServletRequest request, Authentication authentication) throws ParseException {
        Travel travel = new Travel();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date date = formatter.parse(request.getParameter("date"));
        travel.setId(Long.parseLong(request.getParameter("id")));
        travel.setDate(date);
        travel.setCoorDestination(request.getParameter("coorDestination"));
        travel.setCoorOrigin(request.getParameter("coorOrigin"));
        travel.setOrigin(request.getParameter("origin"));
        travel.setDestination(request.getParameter("destination"));
        travel.setPlaces(Integer.parseInt(request.getParameter("places")));
        return travelService.update(travel, authentication);
    }

    @RequestMapping(value="/api/travel/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public boolean  deleteTravel(@PathVariable(value="id") Long id, Authentication authentication){
        return travelService.delete(id, authentication);
    }

    @RequestMapping(value="/api/travel/{id}/exit", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public boolean  personExitTravel(@PathVariable(value="id") Long idTravel, Authentication authentication){
        return travelService.personOutTravel(idTravel, authentication);
    }

    @RequestMapping(value="/api/travel/{id}/join", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public boolean  personJoinTravel(@PathVariable(value="id") Long idTravel, Authentication authentication){
        return travelService.personJoinTravel(idTravel, authentication);
    }

    @RequestMapping(value="/api/travel/{idTravel}/person/{idPerson}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public boolean  deletePersonTravel(@PathVariable(value="idTravel") Long idTravel,
                                       @PathVariable(value="idPerson") Long idPerson,
                                       Authentication authentication){
        return travelService.cancelPersonByTravel(idTravel, idPerson, authentication);
    }

    @RequestMapping(value ="/api/travel/suggestTime", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Boolean  suggestTime(HttpServletRequest request, Authentication authentication) throws ParseException {
        String time = request.getParameter("hour");
        String comment = request.getParameter("comment");
        Long travelId= Long.parseLong(request.getParameter("id"));
        return travelService.suggestTimeChange(travelId, time, comment, authentication);
    }
}
