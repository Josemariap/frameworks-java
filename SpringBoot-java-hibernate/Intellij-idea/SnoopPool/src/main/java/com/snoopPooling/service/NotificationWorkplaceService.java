package com.snoopPooling.service;


public interface NotificationWorkplaceService {

    Boolean sendNotificationWorkplace(String url, String data);
}
