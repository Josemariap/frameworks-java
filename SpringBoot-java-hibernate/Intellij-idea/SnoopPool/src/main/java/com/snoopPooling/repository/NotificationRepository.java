package com.snoopPooling.repository;

public interface NotificationRepository {

    Boolean sendNotificationMail(String mail, String title, String message);
}
