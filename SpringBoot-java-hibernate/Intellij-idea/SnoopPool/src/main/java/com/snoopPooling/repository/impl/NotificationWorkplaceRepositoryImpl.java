package com.snoopPooling.repository.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import com.snoopPooling.repository.NotificationWorkplaceRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
public class NotificationWorkplaceRepositoryImpl implements NotificationWorkplaceRepository {

    @Value("${workplace.api}")
    private String apiWorkplace;
    @Value("${workplace.idgroup}")
    private String idGroupWorkplace;
    @Value("${workplace.token}")
    private String tokenWorkplace;
    @Override
    public Boolean sendNotificationWorkplace(String urlTravel, String data)throws  Exception  {

        String url = apiWorkplace + idGroupWorkplace + "/feed" + tokenWorkplace;
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        String urlParameters = "message=" + data+ "&" + "link=" + urlTravel;
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return true;
    }
}
