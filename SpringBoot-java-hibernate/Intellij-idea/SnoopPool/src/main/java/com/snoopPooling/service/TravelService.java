package com.snoopPooling.service;

import com.snoopPooling.domain.Person;
import com.snoopPooling.domain.Travel;
import org.springframework.security.core.Authentication;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface TravelService {

    Travel save(Travel travel, Authentication authentication);

    Travel update(Travel travel, Authentication authentication);

    Travel getTravelById(Long id);

    List<Travel> getAllTravel();

    Boolean delete(Long idTravel, Authentication authentication);

    List<Travel> getAllTravelBytDate(Date date);

    List<Travel> getAllTravelByDateAndOriginAndDestination(Date date, String origin, String destination);

    List<Travel> findAllByDateAndCoorOriginAndAndCoorDestination(String date, String coorOrigin, String coorDestination) throws ParseException;

    List<Travel> getAllTravelByPerson(Person person);

    Boolean cancelPersonByTravel(Long travelId, Long personId, Authentication authentication);

    Boolean personJoinTravel(Long idTravel, Authentication authentication);

    Boolean personOutTravel(Long idTravel, Authentication authentication);

    Boolean suggestTimeChange(Long idTravel, String time, String comment, Authentication authentication);
}
