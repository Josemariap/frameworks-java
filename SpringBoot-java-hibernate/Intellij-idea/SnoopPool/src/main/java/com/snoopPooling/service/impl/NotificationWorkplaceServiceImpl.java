package com.snoopPooling.service.impl;

import com.snoopPooling.repository.NotificationWorkplaceRepository;
import com.snoopPooling.service.NotificationWorkplaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationWorkplaceServiceImpl implements NotificationWorkplaceService {

    @Autowired
    NotificationWorkplaceRepository notificationWorkplaceRepository;

    @Override
    public Boolean sendNotificationWorkplace(String url, String data) {
        try {
            return notificationWorkplaceRepository.sendNotificationWorkplace(url, data);
        }catch (Exception e) {
            return false;
        }

    }
}
