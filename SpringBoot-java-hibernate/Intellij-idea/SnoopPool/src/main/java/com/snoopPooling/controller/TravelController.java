package com.snoopPooling.controller;

import com.snoopPooling.domain.Person;
import com.snoopPooling.domain.Travel;
import com.snoopPooling.service.PersonService;
import com.snoopPooling.service.TravelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.text.ParseException;

@Controller
public class TravelController {

    @Autowired
    TravelService travelService;

    @Autowired
    PersonService personService;

    @RequestMapping(value="/travel/{id}", method= RequestMethod.GET)
    public String getTravelByIdView(Authentication authentication, Model model, @PathVariable Long id) {
        model.addAttribute("travel", travelService.getTravelById(id));
        model.addAttribute("user", personService.getUserWhitSession(authentication));
        return "site.travel";
    }

    @RequestMapping(value="/travel/new", method= RequestMethod.GET)
    public String getNewTravelView(Authentication authentication, Model model) {
        model.addAttribute("user", personService.getUserWhitSession(authentication));
        return "site.travel.new";
    }

    @RequestMapping(value="/travel/user", method= RequestMethod.GET)
    public String getTravelByUserId(Authentication authentication, Model model) {
        Person person = (Person) personService.getUserWhitSession(authentication);
       // model.addAttribute("travel", travelService.getTravelsByUserId(person.getId()));
        model.addAttribute("user", person);
        return "site.travel.user";
    }

    @RequestMapping(value="/travel/edit/{id}", method= RequestMethod.GET)
    public String getEditMyTravel(Authentication authentication, Model model, @PathVariable Long id) {
        Person person = (Person) personService.getUserWhitSession(authentication);
        Travel travel = travelService.getTravelById(id);
            model.addAttribute("travel", travel);
            model.addAttribute("user", person);
            return "site.travel.edit";
    }

    @RequestMapping(value="/mytravels", method= RequestMethod.GET)
    public String getMyTravels(Authentication authentication, Model model) {
        Person person = (Person) personService.getUserWhitSession(authentication);
        model.addAttribute("user", person);
        model.addAttribute("travels", travelService.getAllTravelByPerson(person));
        return "site.travel.user";
    }

    @RequestMapping(value="/travels/{date}/{coorOrigin}&{coorDestination}/", method= RequestMethod.GET)
    public String getTravelsCustomEmail(Authentication authentication, Model model,
            @PathVariable String date,
            @PathVariable String coorOrigin,
            @PathVariable String coorDestination) throws ParseException {
        Person person = (Person) personService.getUserWhitSession(authentication);
        model.addAttribute("user", person);
        model.addAttribute("travels", travelService.findAllByDateAndCoorOriginAndAndCoorDestination(date, coorOrigin, coorDestination));
        return "site.travel.custom.mail";
    }
}
