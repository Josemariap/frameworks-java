package com.snoopPooling.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "travel")
public class Travel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "DATE")
    private Date date;

    @Column(name = "ORIGIN")
    private String origin;

    @Column(name = "DESTINATION")
    private String destination;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_PERSON")
    private Person person;

    @Column(name = "PLACES")
    private int places;

    @Column(name = "COOR_ORIGIN")
    private String coorOrigin;

    @Column(name = "COOR_DESTINATION")
    private String coorDestination;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="persons_travel",
            joinColumns ={@JoinColumn(name = "ID_TRAVEL", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name= "ID_PERSON")}
    )
    private List<Person> persons;

    public Travel() {
    }

    public Travel(Date date, String origin, String destination, Person person, int places, String coorOrigin, String coorDestination, List<Person> persons) {
        this.date = date;
        this.origin = origin;
        this.destination = destination;
        this.person = person;
        this.places = places;
        this.persons = persons;
        this.coorOrigin = coorOrigin;
        this.coorDestination = coorDestination;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public int getPlaces() {
        return places;
    }

    public void setPlaces(int places) {
        this.places = places;
    }

    public String getCoorOrigin() {
        return coorOrigin;
    }

    public void setCoorOrigin(String coorOrigin) {
        this.coorOrigin = coorOrigin;
    }

    public String getCoorDestination() {
        return coorDestination;
    }

    public void setCoorDestination(String coorDestination) {
        this.coorDestination = coorDestination;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    @Override
    public String toString() {
        return "Travel{" +
                "id=" + id +
                ", date=" + date +
                ", origin='" + origin + '\'' +
                ", destination='" + destination + '\'' +
                ", person=" + person +
                ", places=" + places +
                ", coorOrigin='" + coorOrigin + '\'' +
                ", coorDestination='" + coorDestination + '\'' +
                ", persons=" + persons +
                '}';
    }
}
