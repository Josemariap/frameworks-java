package com.snoopPooling.service;

import java.util.Date;

public interface MessageService {

    String getMessage(String id);

    String buildMailBody(Date date, String origin, String destination, String methodName);

    String buildMailBodySuggestTimeChange(Date date, String origin, String destination, String namePerson, String time, String comment);

    String buildWorkPlaceBody(Date date, String origin, String destination, String methodName);

}
