package com.snoopPooling.service.impl;

import com.snoopPooling.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Service
public class MessageServiceImpl implements MessageService{

    @Autowired
    private MessageSource messageSource;

    private DateFormat hourFormat= new SimpleDateFormat("HH:mm");
    private DateFormat dayFormat = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    public String getMessage(String id) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(id,null,locale);
    }

    @Override
    public String buildMailBody(Date date, String origin, String destination, String methodName) {
        switch (methodName) {
            case "update": return "ACTUALIZACION DE VIAJE\n Origen: " + origin + "\n Destino: " + destination + "\n Fecha: " + dayFormat.format(date) + " a las" + hourFormat.format(date);
            case "delete": return "CANCELACION DE VIAJE\n El viaje con origen: " + origin + "\n Destino: " + destination + "\n Fecha: " + dayFormat.format(date) + " a las" + hourFormat.format(date) + " ha sido cancelado\n";
            case "cancelPersonByTravel": return "USTED HA SIDO CANCELADO DE UN VIAJE\n Ya no esta suscripto al viaje con origen: "  + origin + "\n Destino: " + destination + "\n Fecha: " + dayFormat.format(date) + " a las" + hourFormat.format(date);
            case "personJoinTravel": return "NUEVO USUARIO SUSCRIPTO\n Un nuevo usuario se ha suscripto a un viaje que publicaste con origen: " + origin + "\n Destino: " + destination + "\n Fecha: " + dayFormat.format(date) + " a las" + hourFormat.format(date) + "\n puedes cancelarlo si lo deseas";
            case "personOutTravel": return "UN USUARIO HA SALIDO DE UN VIAJE QUE PUBLICASTE\n Un usuario ha salido de un viaje que publicaste con origen: " + origin + "\n Destino: " + destination + "\n Fecha: " + dayFormat.format(date) + " a las" + hourFormat.format(date);

        }
        throw new DataRetrievalFailureException(getMessage("buildMailBody.trowException"));
    }

    @Override
    public String buildMailBodySuggestTimeChange(Date date, String origin, String destination, String namePerson, String time, String comment) {
        return "SUGERENCIA DE CAMBIO DE HORARIO DE UN VIAJE\n El usuario: " +  namePerson + " sugiere el horario: " + time  + "\n para el viaje con fecha: " + dayFormat.format(date) + " a las" + hourFormat.format(date) + "\n con origen: " + origin + " y destino: " + destination + ".\n Razon: \n" + comment;
    }

    @Override
    public String buildWorkPlaceBody(Date date, String origin, String destination, String methodName) {

        switch (methodName) {
            case "save": return "NUEVO VIAJE\n Origen: " + origin + "\n Destino: " + destination + "\n Fecha: " + dayFormat.format(date) + " a las" + hourFormat.format(date);

        }
        throw new DataRetrievalFailureException(getMessage("buildWorkPlaceBody.trowException"));
    }
}
