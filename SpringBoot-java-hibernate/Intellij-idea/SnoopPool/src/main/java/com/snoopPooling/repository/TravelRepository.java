package com.snoopPooling.repository;

import com.snoopPooling.domain.Person;
import com.snoopPooling.domain.Travel;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface TravelRepository extends CrudRepository<Travel, Long> {
    List<Travel> findAllByDate(Date date);

    List<Travel> findAllByDateAndOriginAndDestination(Date date, String origin, String destination);

    List<Travel> findAllByDateAndCoorOriginAndAndCoorDestination(Date date, String origin, String destination);

    List<Travel> findAllByDateIsGreaterThanOrderByDateAsc(Date date);

    List<Travel> findAllByDateAndPerson(Date date, Person person);

    List<Travel> findAllByDateAndPersonAndIdNot(Date date, Person person, Long id);

    List<Travel> findAllByPerson(Person person);

    Travel findById(Long id);

    List<Travel> findAllByDateAndOriginAndDestinationAndPersonsAndIdNot(Date date, String origin, String destination, Person person, Long id);

}
