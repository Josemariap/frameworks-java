DROP TABLE IF EXISTS persons;
DROP TABLE IF EXISTS persons_travel;
DROP TABLE IF EXISTS travel;
DROP TABLE IF EXISTS invitations;

CREATE TABLE persons (
  ID int IDENTITY NOT NULL PRIMARY KEY ,
  NAME varchar(50) DEFAULT NULL,
  MAIL varchar(50) DEFAULT NULL,
  AVATAR varchar(255) NOT NULL,
  ID_WORKPLACE int NOT NULL
);

CREATE TABLE travel (
  ID int IDENTITY NOT NULL PRIMARY KEY ,
  DATE date DEFAULT NULL,
  ORIGIN varchar(100) DEFAULT NULL,
  DESTINATION varchar(100) DEFAULT NULL,
  ID_PERSON int DEFAULT NULL,
  PLACES int DEFAULT NULL,
  COOR_ORIGIN varchar(250) DEFAULT NULL,
  COOR_DESTINATION varchar(250) DEFAULT NULL,
  CONSTRAINT travel_fk_1 FOREIGN KEY (ID_PERSON) REFERENCES persons (ID)
);

CREATE TABLE persons_travel (
  ID int IDENTITY NOT NULL PRIMARY KEY ,
  ID_PERSON int NOT NULL,
  ID_TRAVEL int NOT NULL,
  CONSTRAINT persons_travel_fk_1 FOREIGN KEY (ID_TRAVEL) REFERENCES travel (ID),
  CONSTRAINT persons_travel_fk_2 FOREIGN KEY (ID_PERSON) REFERENCES persons (ID)
);

CREATE TABLE invitations (
  ID int IDENTITY NOT NULL PRIMARY KEY ,
  ID_PERSON int NOT NULL,
  ID_TRAVEL int NOT NULL,
  CONSTRAINT persons_travel_fk_3 FOREIGN KEY (ID_TRAVEL) REFERENCES travel (ID),
  CONSTRAINT persons_travel_fk_4 FOREIGN KEY (ID_PERSON) REFERENCES persons (ID)
);

INSERT INTO persons (ID, NAME, MAIL, AVATAR, ID_WORKPLACE) VALUES (1, 'Maxi Lopez', 'jpico841@gmail.com', 'C://images/avatar', 1);

INSERT INTO persons (ID, NAME, MAIL, AVATAR, ID_WORKPLACE) VALUES (2, 'Roberto Perez', 'maxi.lopez@gmail.com', 'C://images/avatar', 1);

INSERT INTO persons (ID, NAME, MAIL, AVATAR, ID_WORKPLACE) VALUES (3, 'Roberto Perez', 'maximiliano.caba@snoopconsulting.com', 'C://images/avatar', 1);


INSERT INTO travel (ID, DATE, ORIGIN, DESTINATION, ID_PERSON, PLACES, COOR_ORIGIN, COOR_DESTINATION)
            VALUES (350, CURRENT_DATE, 'Sarmiento 1230', 'La Plata', 1, 5, '@googleOrigin', '@googleDestination');

INSERT INTO travel (ID, DATE, ORIGIN, DESTINATION, ID_PERSON, PLACES, COOR_ORIGIN, COOR_DESTINATION)
            VALUES (351, CURRENT_DATE, 'Sarmiento 1230', 'La Plata', 2, 5, '@googleOrigin', '@googleDestination');

INSERT INTO travel (ID, DATE, ORIGIN, DESTINATION, ID_PERSON, PLACES, COOR_ORIGIN, COOR_DESTINATION)
            VALUES (352, CURRENT_DATE, 'La Plata', 'Sarmiento 1230', 1, 5, '@googleOrigin', '@googleDestination');

INSERT INTO travel (ID, DATE, ORIGIN, DESTINATION, ID_PERSON, PLACES, COOR_ORIGIN, COOR_DESTINATION)
            VALUES (353, CURRENT_DATE + 1 DAY , 'La Plata', 'Sarmiento 1230', 1, 5, '@googleOrigin', '@googleDestination');

INSERT INTO travel (ID, DATE, ORIGIN, DESTINATION, ID_PERSON, PLACES, COOR_ORIGIN, COOR_DESTINATION)
            VALUES (400, CURRENT_DATE + 3 DAY , 'Sarmiento 1230', 'La Plata', 1, 5, '@googleOrigin', '@googleDestination');

INSERT INTO travel (ID, DATE, ORIGIN, DESTINATION, ID_PERSON, PLACES, COOR_ORIGIN, COOR_DESTINATION)
            VALUES (500, CURRENT_DATE + 2 DAY, 'Sarmiento 1230', 'La Plata', 3, 4, '@googleOrigin', '@googleDestination');

INSERT INTO travel (ID, DATE, ORIGIN, DESTINATION, ID_PERSON, PLACES, COOR_ORIGIN, COOR_DESTINATION)
            VALUES (501, CURRENT_DATE + 3 DAY, 'Sarmiento 1230', 'La Plata', 3, 4, '@googleOrigin', '@googleDestination');

INSERT INTO travel (ID, DATE, ORIGIN, DESTINATION, ID_PERSON, PLACES, COOR_ORIGIN, COOR_DESTINATION)
VALUES (601, CURRENT_DATE + 3 DAY, 'Sarmiento 1230', 'La Plata', 1, 4, '@googleOrigin', '@googleDestination');

INSERT INTO travel (ID, DATE, ORIGIN, DESTINATION, ID_PERSON, PLACES, COOR_ORIGIN, COOR_DESTINATION)
VALUES (602, CURRENT_DATE + 3 DAY, 'Sarmiento 1230', 'La Plata', 2, 4, '@googleOrigin', '@googleDestination');

INSERT INTO travel (ID, DATE, ORIGIN, DESTINATION, ID_PERSON, PLACES, COOR_ORIGIN, COOR_DESTINATION)
VALUES (603, CURRENT_DATE, 'Sarmiento 1230', 'La Plata', 3, 4, '-34.6044226,-58.3896719,15.78z?hl', '-34.6044226,-58.3896719,15.78z?hl');

INSERT INTO persons_travel (ID, ID_PERSON, ID_TRAVEL) VALUES (1,3, 601);

INSERT INTO persons_travel (ID, ID_PERSON, ID_TRAVEL) VALUES (2,2, 500);

INSERT INTO persons_travel (ID, ID_PERSON, ID_TRAVEL) VALUES (3,2, 350);

INSERT INTO persons_travel (ID, ID_PERSON, ID_TRAVEL) VALUES (4,1, 501);