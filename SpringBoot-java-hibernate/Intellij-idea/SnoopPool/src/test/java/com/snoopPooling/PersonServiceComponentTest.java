package com.snoopPooling;

import com.snoopPooling.domain.Person;
import com.snoopPooling.service.PersonService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationStart.class)
@Transactional
public class PersonServiceComponentTest {

     @Autowired
     private PersonService personService;

    Authentication auth = null;

    @Before
    public void createNewSession() {
        final OAuth2AccessToken token = new DefaultOAuth2AccessToken("FOO");
        final ClientDetails client = new BaseClientDetails("client", null, "read", "client_credentials", "ROLE_CLIENT");
        final OAuth2Authentication authentication = new OAuth2Authentication(
                new TokenRequest(null, "client", null, "client_credentials").createOAuth2Request(client), null);
        //NO CAMBIAR ESTO
        authentication.setDetails("{'email':'maximiliano.caba@snoopconsulting.com','email_verified':'true'}");
        auth = authentication;
    }

     @Test
     public void findPersonById_returnPersonById(){
         Person p = new Person();
         p.setId(1l);
         assertEquals(p.getId(), personService.getPersonById(1l).getId());
     }

     @Test
     public void findPersonByMail_returnPersonByMail(){
         Person p = new Person();
         p.setMail("maxi.lopez@gmail.com");
         assertEquals(p.getMail(), personService.getPersonByMail("maxi.lopez@gmail.com").getMail());
     }

     @Test
     public void findPersonByMail_returnObjectPerson(){
         assertNotNull(personService.getPersonByMail("maxi.lopez@gmail.com"));
     }

}