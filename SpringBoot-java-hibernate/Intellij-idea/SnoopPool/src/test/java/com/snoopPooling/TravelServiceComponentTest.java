package com.snoopPooling;

import com.snoopPooling.domain.Person;
import com.snoopPooling.domain.Travel;
import com.snoopPooling.service.PersonService;
import com.snoopPooling.service.TravelService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationStart.class)
@Transactional
public class TravelServiceComponentTest {

    @Autowired
    private TravelService travelService;
    @Autowired
    private PersonService personService;

    Authentication auth = null;

    @Before
    public void createNewSession() {
        final OAuth2AccessToken token = new DefaultOAuth2AccessToken("FOO");
        final ClientDetails client = new BaseClientDetails("client", null, "read", "client_credentials", "ROLE_CLIENT");
        final OAuth2Authentication authentication = new OAuth2Authentication(
                new TokenRequest(null, "client", null, "client_credentials").createOAuth2Request(client), null);
        //NO CAMBIAR ESTO
        authentication.setDetails("{'email':'maximiliano.caba@snoopconsulting.com','email_verified':'true'}");
        auth = authentication;
    }

    @Test
    public void saveTravel_returnTravel(){
        Travel travel = new Travel();
        travel.setDestination("La plata");
        travel.setOrigin("Sarmiento 1230 Capital");
        travel.setPlaces(2);
        travel.setDate(new Date());
        travel.setCoorOrigin("-34.6044226,-58.3896719,15.78z?hl");
        travel.setCoorDestination("-34.6044226,-58.3896719,15.78z?hl");
        assertEquals(travel, travelService.save(travel, auth));
    }

    @Test(expected = DataRetrievalFailureException.class)
    public void saveTravel_sameTrayectByUserAndDayExist_dontSave(){
        Travel travel = new Travel();
        travel.setDate(new Date());
        travel.setDestination("La Plata");
        travel.setOrigin("Sarmiento 1230");
        travel.setCoorOrigin("-34.6044226,-58.3896719,15.78z?hl");
        travel.setCoorDestination("-34.6044226,-58.3896719,15.78z?hl");
        travel.setPlaces(4);
        travelService.save(travel,auth);
    }

    @Test(expected = DataRetrievalFailureException.class)
    public void validation_returnException(){
        Travel travel = new Travel();
        travelService.save(travel, auth);
    }

    @Test
    public void findTravel_returnTravel(){
        assertNotNull(travelService.getTravelById(350L));
    }

    @Test
    public void updateTravel_travelExist_returnUpdateTravel(){
        Travel travel = new Travel();
        travel.setId(500L);
        travel.setDate(new Date());
        travel.setPerson(personService.getPersonById(3L));
        travel.setDestination("Sarmiento 1230");
        travel.setOrigin("La Plata");
        travel.setCoorOrigin("-34.6044226,-58.3896719,15.78z?hl");
        travel.setCoorDestination("-34.6044226,-58.3896719,15.78z?hl");
        travel.setPlaces(5);
        assertNotNull(travelService.update(travel, auth));
    }

    @Test(expected = DataRetrievalFailureException.class)
    public void updateTravel_setDestinationNull_returnException(){
        Travel travel = travelService.getTravelById(500l);
        travel.setOrigin("Sarmiento 1230 capital");
        travel.setPlaces(4);
        travel.setDestination(null);
        travelService.update(travel, auth);
    }

    @Test(expected = DataRetrievalFailureException.class)
    public void updateTravel_travelPersonNotEqualsPersonSession_returnException(){
        Travel travel = travelService.getTravelById(350l);
        travelService.update(travel, auth);
    }

    @Test
    public void  deleteTravel_travelExist_deleteSuccess(){
        assertEquals(true, travelService.delete(501l, auth));
    }

    @Test(expected = DataRetrievalFailureException.class)
    public void  deleteTravel_travelExistAndNotUserAutorized_launchException(){
        travelService.delete(350l, auth);
    }

    @Test
    public void  getAllTravels_travelsExist_listTravelsAfterDateNow(){
        List<Travel> travels = travelService.getAllTravel();
        Date dateNow = new Date();
        assertNotNull(travelService.getAllTravel());
        for (Travel travel: travels) {
            Date dateTravel = travel.getDate();
            assertTrue(dateNow.before(dateTravel));
        }
    }

    @Test
    public void  getAllTravel_travelsExist_returnListGreatDateAsc(){
        List<Travel> travels = travelService.getAllTravel();
        assertNotNull(travelService.getAllTravel());
        int index = 0;
        for (Travel travel: travels) {
            Date dateTravel = travel.getDate();
            int nextTravel = index + 1;
            if(travels.size() < index){
            Date dataTravelAfter = travels.get(nextTravel).getDate();
            assertTrue(dateTravel.before(dataTravelAfter) || dateTravel.equals(dataTravelAfter));
            index++;
            }
        }
    }

    @Test
    public void getTravel_returnListByDate(){
        Date date = new Date();
        assertEquals(false, travelService.getAllTravelBytDate(date).isEmpty());
    }

    @Test
    public void getTravel_returnListByDateAndOriginAndDestination(){
        Date date = new Date();
        assertEquals(3, travelService.getAllTravelByDateAndOriginAndDestination(date, "Sarmiento 1230", "La Plata").size());
    }

    @Test
    public void getPersonByTravel_returnPersons(){
        assertEquals("jpico841@gmail.com",  travelService.getTravelById(350l).getPerson().getMail());

    }

    @Test
    public  void getTravelByPerson_returnTravel(){
        Person person = personService.getPersonById(1L);
        List<Travel> travels= travelService.getAllTravelByPerson(person);
        assertEquals(5, travels.size());
    }

    @Test
    public void cancelPersonByTravel_returnBoolean(){
        assertTrue(travelService.cancelPersonByTravel(500L, 2L, auth));
    }

    @Test(expected = DataRetrievalFailureException.class)
    public void cancelPersonByTravel_returnException(){
        travelService.cancelPersonByTravel(350L, 2L, auth);
    }

    // //el usuario se agrega a un viaje
    @Test
    public void joinPersonToTravel_returnTrue(){
        assertEquals(true, travelService.personJoinTravel(350L, auth));
    }

    @Test(expected = DataRetrievalFailureException.class)
    public void joinPersonToTravel_returnException(){
        assertEquals(true, travelService.personJoinTravel(602L, auth));
    }

    //el usuario se sale de un viaje al que se suscribio anteriormente
    @Test(expected = DataRetrievalFailureException.class)
    public void  outPersonToTravel_returnException(){
        travelService.personOutTravel(350L , auth);
    }

    @Test
    public void outPersonToTravel_returnTrue(){
        travelService.personJoinTravel(350L, auth);
        assertEquals(true, travelService.personOutTravel(350L , auth));
    }

    @Test
    public void  suggestTimeChange_void(){
        assertEquals(true, travelService.suggestTimeChange(601L, "1:00 pm", "Thanks", auth));
    }

}
