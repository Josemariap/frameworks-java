package com.snoopPooling;


import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetupTest;
import com.snoopPooling.service.NotificationService;
import com.snoopPooling.service.NotificationWorkplaceService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationStart.class)
@Transactional
public class NotificationServiceComponentTest {


    @Autowired
    private NotificationService notificationService;
    private GreenMail greenMail;
    @Autowired
    NotificationWorkplaceService notificationWorkplaceService;

    @Before
    public void startMailServer() {
        greenMail = new GreenMail(ServerSetupTest.SMTP);
        greenMail.start();
    }

    @Test
    public void greenMailServer_returnMsj() {
        GreenMailUtil.sendTextEmailTest("to@localhost.com", "from@localhost.com", "some subject", "some body");
        assertEquals(1, greenMail.getReceivedMessages().length);
        greenMail.stop();
    }

    @Test
    public void sendNotification_returnBoolean() {
        assertTrue(notificationService.sendNotificationsMail("jpico841@gmail.com", "Cancelacion de viaje de SnoopPool", "El viaje con destino a La Plata para el dia 16/12/2017 ha sido cancelado"));
    }

    @Test
    public  void  sendNotificationWorkplace_returnBoolean(){
        assertEquals(true, notificationWorkplaceService.sendNotificationWorkplace("http://google.com.ar", "origen, destino y fecha"));
    }

}
