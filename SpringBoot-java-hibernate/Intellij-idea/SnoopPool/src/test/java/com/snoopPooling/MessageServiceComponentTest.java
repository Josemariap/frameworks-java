package com.snoopPooling;

import com.snoopPooling.service.MessageService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.NoSuchMessageException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationStart.class)
@Transactional
public class MessageServiceComponentTest {


    @Autowired
    private MessageService messageService;

    Authentication auth = null;

    @Before
    public void createNewSession() {
        final OAuth2AccessToken token = new DefaultOAuth2AccessToken("FOO");
        final ClientDetails client = new BaseClientDetails("client", null, "read", "client_credentials", "ROLE_CLIENT");
        final OAuth2Authentication authentication = new OAuth2Authentication(
                new TokenRequest(null, "client", null, "client_credentials").createOAuth2Request(client), null);
        //NO CAMBIAR ESTO
        authentication.setDetails("{'email':'maximiliano.caba@snoopconsulting.com','email_verified':'true'}");
        auth = authentication;
    }

    @Test
    public void getMessage_messageExist_returnMessage (){
        assertNotNull(messageService.getMessage("save.trowException.existSimilarTravel"));
    }

    @Test(expected = NoSuchMessageException.class)
    public void getMessage_messageDontExist_returnException(){
        System.out.println(messageService.getMessage("MENSAJERELOCOOO"));
    }

    @Test
    public void getMessageBuildBodyWorkPlaceMethodSAVE_messageExist_returnMessage (){
        Date date = new Date();
        assertNotNull(messageService.buildWorkPlaceBody(date, "origen", "destination", "save"));
    }

    @Test(expected = DataRetrievalFailureException.class)
    public void getMessageBuildBodyWorkPlace_invalidMethod_returnException(){
        Date date = new Date();
        messageService.buildWorkPlaceBody(date, "origen", "destination", "METODORELOCO");
    }


    @Test(expected = DataRetrievalFailureException.class)
    public void getMessagebuildMailBody_invalidMethod_returnException(){
        Date date = new Date();
        messageService.buildMailBody(date, "origen", "destination", "METODORELOCO");
    }
}
