package com.snoopPooling;

import com.snoopPooling.Utils.Validator;
import com.snoopPooling.domain.Travel;
import com.snoopPooling.service.PersonService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationStart.class)
@Transactional
public class ValidationServiceComponentTest {

    @Autowired
    PersonService personService;

    @Test
    public void validatePlacesLowerPersons_returnTrue(){
        assertEquals(true, Validator.validatePlaceTravel(4, 3));
    }

    @Test(expected = DataRetrievalFailureException.class)
    public void validatePlacesLowerPersons_returnException(){
         Validator.validatePlaceTravel(1, 3);
    }

    @Test
    public void  validateOriginTravel_returnFalse(){
        Travel travel = new Travel();
        travel.setDate(new Date());
        travel.setPerson(personService.getPersonById(1L));
        travel.setDestination("La Plata");
        travel.setCoorOrigin("-34.6044226,-58.3896719,15.78z?hl");
        travel.setCoorDestination("-34.6044226,-58.3896719,15.78z?hl");
        travel.setPlaces(5);
        assertEquals(false, Validator.validateTravel(travel));
    }

    @Test
    public void  validateDestinationTravel_returnFalse(){
        Travel travel = new Travel();
        travel.setDate(new Date());
        travel.setPerson(personService.getPersonById(1L));
        travel.setOrigin("Sarmiento 1230");
        travel.setCoorOrigin("-34.6044226,-58.3896719,15.78z?hl");
        travel.setCoorDestination("-34.6044226,-58.3896719,15.78z?hl");
        travel.setPlaces(5);
        assertEquals(false, Validator.validateTravel(travel));
    }

    @Test
    public void  validatePlacesTravel_returnFalse(){
        Travel travel = new Travel();
        travel.setDate(new Date());
        travel.setPerson(personService.getPersonById(1L));
        travel.setDestination("La Plata");
        travel.setOrigin("Sarmiento 1230");
        travel.setCoorOrigin("-34.6044226,-58.3896719,15.78z?hl");
        travel.setCoorDestination("-34.6044226,-58.3896719,15.78z?hl");
        assertEquals(false, Validator.validateTravel(travel));
    }

    @Test
    public void  validateDateTravel_returnFalse(){
        Travel travel = new Travel();
        travel.setPerson(personService.getPersonById(1L));
        travel.setDestination("La Plata");
        travel.setOrigin("Sarmiento 1230");
        travel.setCoorOrigin("-34.6044226,-58.3896719,15.78z?hl");
        travel.setCoorDestination("-34.6044226,-58.3896719,15.78z?hl");
        travel.setPlaces(5);
        assertEquals(false, Validator.validateTravel(travel));
    }

    @Test
    public void  validatePersonTravel_returnFalse(){
        Travel travel = new Travel();
        travel.setPerson(personService.getPersonById(1L));
        travel.setDate(new Date());
        travel.setDestination("La Plata");
        travel.setOrigin("Sarmiento 1230");
        travel.setCoorOrigin("-34.6044226,-58.3896719,15.78z?hl");
        travel.setCoorDestination("-34.6044226,-58.3896719,15.78z?hl");
        travel.setPlaces(5);
        assertEquals(false, Validator.validateTravel(travel));
    }

    @Test
    public void  validateCoorOriginTravel_returnFalse(){
        Travel travel = new Travel();
        travel.setPerson(personService.getPersonById(1L));
        travel.setDate(new Date());
        travel.setDestination("La Plata");
        travel.setOrigin("Sarmiento 1230");
        travel.setCoorDestination("-34.6044226,-58.3896719,15.78z?hl");
        travel.setPlaces(5);
        assertEquals(false, Validator.validateTravel(travel));
    }

    @Test
    public void  validateCoorDestinationTravel_returnFalse(){
        Travel travel = new Travel();
        travel.setPerson(personService.getPersonById(1L));
        travel.setDate(new Date());
        travel.setDestination("La Plata");
        travel.setOrigin("Sarmiento 1230");
        travel.setCoorOrigin("-34.6044226,-58.3896719,15.78z?hl");
        travel.setPlaces(5);
        assertEquals(false, Validator.validateTravel(travel));
    }
}
