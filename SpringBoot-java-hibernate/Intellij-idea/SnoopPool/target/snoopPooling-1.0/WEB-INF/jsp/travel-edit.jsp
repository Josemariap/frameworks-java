<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="container">
    <br>
    <br>
    <div class="row" id="main">
        <div class="col-md-4 well" id="leftPanel">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <img src="<c:url value="/"/>image/map-car.jpg" alt="Texto Alternativo" class="img-circle img-thumbnail">
                        <h2><spring:message code="travel.edit.title"/></h2>
                        <p><spring:message code="travel.edit.description"/></p>
                        </div>
                </div>
            </div>
        </div>

        <div class="col-md-8 well" id="rightPanel">
            <div class="row">
                <div class="col-md-12">
                    <form role="form">
                        <input type="hidden" id="idTravel" value="${travel.id}">
                        <h2><spring:message code="travel.edit.header.form.1"/><small> <spring:message code="travel.edit.header.form.2"/></small></h2>
                        <hr class="colorgraph">

                        <div class="row">

                            <div id="fechaErronea" class="alert alert-warning hide">
                                <strong><spring:message code="travel.edit.error"/></strong> <spring:message code="travel.edit.error.date"/>
                                .
                            </div>

                            <div id="errorServidorEditar" class="alert alert-warning hide">
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="hidden" id="dateTravel" value="<fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${travel.date}"/>">
                                    <label for="date"><spring:message code="travel.edit.form.date"/></label>
                                    <input type="datetime-local" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${travel.date}"/>T<fmt:formatDate pattern="HH:mm" value="${travel.date}"/>" name="date" id="date" class="form-control input-lg" placeholder="Fecha y Hora" tabindex="1">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group" id="cantPlaces" >
                                    <input type="hidden" id="placesTravel" value="${travel.places}">
                                    <label for="places"><spring:message code="travel.edit.form.place"/></label>
                                        <select class="form-control input-lg" id="places" name="places">
                                            <option id="place_1">1</option>
                                            <option id="place_2">2</option>
                                            <option id="place_3">3</option>
                                            <option id="place_4">4</option>
                                            <option id="place_5">5</option>
                                            <option id="place_6">6</option>
                                        </select>
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <input type="hidden" id="originTravel" value="${travel.origin}">
                                <input type="hidden" id="destinationTravel" value="${travel.destination}">
                                <label for="origin-destination"><spring:message code="travel.edit.form.originDestination"/></label>
                                <select class="form-control input-lg" id="origin-destination" name="origin-destination">
                                    <option id="bs-plata">Buenos Aires - La plata</option>
                                    <option id="plata-bs">La plata - Buenos Aires</option>
                                </select>
                            </div>
                        </div>

                        <hr class="colorgraph">
                        <div class="row">
                            <div class="col-xs-12 col-md-6"></div>
                            <div class="col-xs-12 col-md-6"><a id="botonEditarViaje" class="btn btn-success btn-block btn-lg"><spring:message code="travel.edit.form.button"/></a></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



