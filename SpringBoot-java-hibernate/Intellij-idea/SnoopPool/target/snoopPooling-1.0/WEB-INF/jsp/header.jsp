<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="<c:url value="/"/>">SNOOPOOLING</a>
        </div>
        <sec:authorize access="isAnonymous()">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <form action="<c:url value="/"/>google/login" method="GET">
                            <div class="login-box-btm text-center">
                                <button class="loginBtn loginBtn--google">
                                    LogIn
                                </button>
                            </div>
                        </form>
                    </li>
                </ul>
            </div>
        </sec:authorize>
        <sec:authorize access="isAuthenticated()">
            <c:choose>
                <c:when test="${user==null}">
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <form action="<c:url value="/"/>logout" method="POST">
                                    <div class="login-box-btm text-center">
                                        <button class="loginBtn loginBtn--google">
                                            Logout
                                        </button>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <img src="${user.avatar}" class="img-circle"/>
                                </a>
                                <ul class="dropdown-menu dropdown-cart" role="menu">

                                    <li>
                                        <a href="<c:url value="/"/>">
                                            <span class="item">
                                                <span class="item-left">
                                                    <img src="<c:url value="/"/>image/home.svg" alt=""/>
                                                        <span class="item-info">
                                                            <span>Pagina</span>
                                                            <span>Inicio</span>
                                                        </span>
                                                </span>
                                            </span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<c:url value="/"/>travel/new">
                                            <span class="item">
                                                <span class="item-left">
                                                    <img src="<c:url value="/"/>image/new-travel.png" alt=""/>
                                                        <span class="item-info">
                                                            <span>Nuevo</span>
                                                            <span>viaje</span>
                                                        </span>
                                                </span>
                                            </span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<c:url value="/"/>mytravels">
                                            <span class="item">
                                                <span class="item-left">
                                                    <img src="<c:url value="/"/>image/my-travel.png" alt=""/>
                                                        <span class="item-info">
                                                            <span>Mis</span>
                                                            <span>viaje</span>
                                                        </span>
                                                </span>
                                            </span>
                                        </a>
                                    </li>

                                    <li class="divider"></li>
                                    <li>
                                        <form action="<c:url value="/"/>logout" method="POST">
                                            <div class="login-box-btm text-center">
                                                <button class="loginBtn loginBtn--google">
                                                    Logout
                                                </button>
                                            </div>
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </c:otherwise>
            </c:choose>
        </sec:authorize>
    </div>
</nav>