<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:choose>
    <c:when test="${empty travels}">
        <!-- TODO colocar imagen y boton agregar nuevo travel -->
    </c:when>
    <c:otherwise>
        <c:forEach items="${travels}" var="travel">
            <div id="travel-body" data-id=${travel.id} class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-offset-2 col-md-8">
                        <ul class="event-list">

                            <li>
                                <time>
                                    <span class="day">
                                        <fmt:formatDate pattern="d" value="${travel.date}"/>
                                    </span>
                                    <span class="month">
                                        <fmt:formatDate pattern="MMM" dateStyle="long" value="${travel.date}"/>
                                    </span>
                                </time>
                                <div class="info">
                                    <h2 class="hour">
                                        <div class="title-list">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            <spring:message code="travel.hour.exit"/> <fmt:formatDate
                                                pattern="H" value="${travel.date}"/>:<fmt:formatDate
                                                pattern="m" value="${travel.date}"/>
                                        </div>
                                    </h2>
                                    <ul>
                                        <li style="width:30%;">${travel.origin} <span
                                                class="fa fa-car"></span></li>
                                        <li style="width:30%;">${travel.destination} <span
                                                class="fa fa-map-marker"></span></li>
                                        <li style="width:20%;">${travel.places} <span
                                                class="fa fa-users"></span></li>
                                        <li style="width:20%;">${fn:length(travel.persons)} <span
                                                class="fa fa-handshake-o"></span></li>
                                    </ul>
                                </div>
                                <div class="social">
                                    <ul>
                                        <li class="twitter" style="width:33%;"><a
                                                href="<c:url value="/travel/${travel.id}"/>"><span
                                                class="fa fa-search"></span></a></li>
                                        <li class="facebook" style="width:33%;"><a href="#facebook"><span
                                                class="fa fa-facebook"></span></a></li>
                                        <li class="star-user">
                                            <c:if test="${travel.person.id == user.id}">
                                                <span class="fa fa-star icon-star-user"></span>
                                            </c:if>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </c:forEach>
    </c:otherwise>
</c:choose>



