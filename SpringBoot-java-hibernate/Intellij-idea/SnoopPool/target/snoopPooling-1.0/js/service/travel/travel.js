snoopooling.service.travel = (function () {

    var uriTravel = snoopooling.service.uriTravel();
    var uriPerson = snoopooling.service.uriPerson();
    var uriApi = snoopooling.service.uriApi();
    var url = snoopooling.urlBase();

    function crear(travel) {
        var settings = {
            url: url + uriApi + uriTravel,
            method: "POST",
            headers: {
                "content-type": "application/x-www-form-urlencoded",
            },
            data: travel
        };
        return $.ajax(settings)
    }

    function editar(travel) {
        var settings = {
            url: url + uriApi + uriTravel + travel.id,
            method: "POST",
            headers: {
                "content-type": "application/x-www-form-urlencoded",
            },
            data: travel
        };
        return $.ajax(settings)
    }

    function eliminar(travel) {
        return $.ajax({
            contentType: 'application/json',
            url: url + uriApi + uriTravel + travel.id,
            method: "DELETE",
        })
    }

    function salirDeViaje(travel) {
        var settings = {
            contentType: 'application/json',
            url: url + uriApi + uriTravel + travel.id + "/exit",
            method: "PUT"
        };
        return $.ajax(settings)
    }

    function ingresarViaje(travel) {
        var settings = {
            contentType: 'application/json',
            url: url + uriApi + uriTravel + travel.id + "/join",
            method: "PUT"

        };
        return $.ajax(settings)
    }

    function borrarPersonaDeViaje(data) {
        var settings = {
            contentType: 'application/json',
            url: url + uriApi + uriTravel + data.travel.id + "/" + uriPerson + data.person.id,
            method: "PUT"
        };
        return $.ajax(settings)
    }

    function suggestTimeAndComment(data) {
        var settings = {
            url: url + uriApi + uriTravel + "/suggestTime",
            method: "POST",
            headers: {
                "content-type": "application/x-www-form-urlencoded"
            },
            data: data
        };
        return $.ajax(settings)
    }

    return {
        crear: crear,
        editar: editar,
        eliminar: eliminar,
        salirDeViaje: salirDeViaje,
        ingresarViaje: ingresarViaje,
        borrarPersonaDeViaje: borrarPersonaDeViaje,
        suggestTimeAndComment: suggestTimeAndComment
    };

})();

