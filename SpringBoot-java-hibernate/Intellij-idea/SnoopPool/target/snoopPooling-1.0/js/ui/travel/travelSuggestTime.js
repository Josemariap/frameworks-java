snoopooling.ui.travelSuggestTime = (function () {

    var $botonConfirmar= $("#botonConfirmarHoraSugerida");
    var $spinner = $("#loadingSpinnerSuggestTime");
    var $divErrorServer = $("#errorServer");
    var $divSuggestTime = $("#suggestTime");
    var $divModalContext = $("#modalContext");
    var $divSuccessSuggestTime = $("#successSuggestTime");
    var $divErrorValidacion = $("#errorValidacion");

    function init() {
        bindearEventosBotenes();
        mostrarDivHorasugerida();
        resetearModal();
        salirSugerirHora();
    }

    function bindearEventosBotenes() {
        $("#botonConfirmarHoraSugerida").on("click", function () {
            $botonConfirmar.addClass("disabled");
            $spinner.removeClass("hide");
            $divErrorServer.addClass("hide");
            var travelId = document.getElementById('botonConfirmarHoraSugerida').value;
            var hour =  document.getElementById('numberHour').value;
            var min = document.getElementById('numberMinutes').value;
            var hourAndMinutes = hour + " : " + min;
            var comment =  document.getElementById('textArea').value.length;
            if(validarCamposFormSuggest(hour, min, comment)) {
                suggestTime(travelId, hourAndMinutes, comment);
            }else{
                $divErrorValidacion.removeClass("hide");
                $spinner.addClass("hide");
                $botonConfirmar.removeClass("disabled");
            }

    })
    }
    
    function  suggestTime(travelId, hour, comment) {
        var suggestNewTime={
            "id": travelId,
            "hour": hour,
            "comment": comment
        };
        snoopooling.service.travel.suggestTimeAndComment(suggestNewTime).done(function () {
            sugerenciaExitosa();
        }).fail(function (error) {
            mostrarMensajeErrorServidor(error.responseJSON.message)
        })
    }

     function sugerenciaExitosa(){
        $spinner.addClass("hide");
        $botonConfirmar.removeClass("disabled");
        $divSuggestTime.addClass("hide");
        $divModalContext.removeClass("hide");
        $divSuccessSuggestTime.removeClass("hide");
        limpiarCamposFormSuggest();
     }

    function mostrarMensajeErrorServidor(error){
        $botonConfirmar.removeClass("disabled")
        $spinner.addClass("hide");
        $divErrorServer.removeClass("hide")
        document.getElementById('errorServer').innerHTML = error;
        limpiarCamposFormSuggest();
    }

    function mostrarDivHorasugerida() {
        $("#botonSugerirHora").on("click", function () {
            $("#modalContext").addClass("hide");
            $("#suggestTime").removeClass("hide");
            $divSuccessSuggestTime.addClass("hide");
            $divErrorValidacion.addClass("hide");
            limpiarCamposFormSuggest();
        })
    }

    function  resetearModal() {
        $('#modalUSer').on('hidden.bs.modal', function () {
            $("#modalContext").removeClass("hide");
            $("#suggestTime").addClass("hide");
            $botonConfirmar.removeClass("disabled");
            $spinner.addClass("hide");
            $divSuccessSuggestTime.addClass("hide");
            $divErrorValidacion.addClass("hide");
        })
    }

    function limpiarCamposFormSuggest(){
        $('#textArea').val("");
        $('#numberHour').val("");
        $('#numberMinutes').val("");
    }

    function salirSugerirHora(){
        $("#botonVolver").on("click", function () {
            limpiarCamposFormSuggest();
            $divSuggestTime.addClass("hide");
            $divModalContext.removeClass("hide");
        })
    }

    function validarCamposFormSuggest(inputHour, inputMinutes, inputComment){
        if(inputHour >= 0 & inputHour <= 23 & inputMinutes >= 0 & inputMinutes <= 59 & inputComment >= 8 ){
            return true;
        }else{
            return false;
        }
    }



    return {
        init: init
    };

})();

$(document).on("ready", function () {
    snoopooling.ui.travelSuggestTime.init();


});
