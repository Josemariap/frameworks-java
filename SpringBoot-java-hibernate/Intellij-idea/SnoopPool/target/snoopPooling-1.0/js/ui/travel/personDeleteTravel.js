snoopooling.ui.personDeleteTravel = (function () {

    var urlTravel = snoopooling.urlBase() + snoopooling.service.uriTravel();

    var $divErrorServidor = $("#errorSalirViajeServidor");

    var $buttonDeletePersonravel =  $("#botonEliminarUsuarioViaje");

    var $spinner = $("#loadingSpinnerDeleteUser");

    function init() {
        bindearEventosABotones();
    }

    function bindearEventosABotones() {
        $buttonDeletePersonravel.on("click", function () {
            $divErrorServidor.addClass("hide");
            $buttonDeletePersonravel.addClass("disabled");
            $spinner.removeClass("hide");
            var travelId = document.getElementById('travelId').value;
            var personId = document.getElementById('personId').value;
            eliminarUsuarioDeViaje(travelId, personId)
        })
    }

    function eliminarUsuarioDeViaje(travelId, personId) {

        var data = {
            travel:{
                id: travelId
            },
            person: {
                id: personId
            }
        };

        snoopooling.service.travel.borrarPersonaDeViaje(data).done(function () {
            redireccionarAUrlRetorno(travelId)
        }).fail(function (error) {
            mostrarMensajeErrorServidor(error.responseJSON.message)
        })
    }

    function redireccionarAUrlRetorno(id) {
        $spinner.addClass("hide");
        window.location.href = urlTravel + id;
    }

    function mostrarMensajeErrorServidor(error) {
        $divErrorServidor.removeClass("hide");
        $spinner.addClass("hide");
        document.getElementById('errorSalirViajeServidor').innerHTML = error;
    }

    return {
        init: init
    };

})();

$(document).on("ready", function () {
    snoopooling.ui.personDeleteTravel.init();
    $('#modalUSer').on('hidden.bs.modal', function () {
        $("#errorSalirViajeServidor").addClass("hide");
        $("#loadingSpinnerDeleteUser").addClass("hide");
    })
});