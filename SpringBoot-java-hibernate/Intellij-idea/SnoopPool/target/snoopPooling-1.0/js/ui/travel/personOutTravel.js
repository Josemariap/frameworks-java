snoopooling.ui.personOutTravel = (function () {

    var urlTravel = snoopooling.urlBase() + snoopooling.service.uriTravel();

    var $divErrorServidor = $("#errorSalirViajeServidor");

    var $buttonExitTravel =  $("#botonSalirViaje");

    var $spinner = $("#loadingSpinnerExit");

    function init() {
        bindearEventosABotones();
    }

    function bindearEventosABotones() {
        $buttonExitTravel.on("click", function () {
            $divErrorServidor.addClass("hide");
            $buttonExitTravel.addClass("disabled");
            $spinner.removeClass("hide");
            var travelId = document.getElementById('botonSalirViaje').value;

            salirViaje(travelId)

        })
    }

    function salirViaje(travelId) {

        snoopooling.service.travel.salirDeViaje({"id": travelId}).done(function () {
            redireccionarAUrlRetorno(travelId)
        }).fail(function (error) {
            mostrarMensajeErrorServidor(error.responseJSON.message)
        })
    }

    function redireccionarAUrlRetorno(id) {
        $spinner.addClass("hide");
        window.location.href = urlTravel + id;
    }

    function mostrarMensajeErrorServidor(error) {
        $divErrorServidor.removeClass("hide");
        $spinner.addClass("hide");
        document.getElementById('errorSalirViajeServidor').innerHTML = error;
    }

    return {
        init: init
    };

})();

$(document).on("ready", function () {
    snoopooling.ui.personOutTravel.init();
    $('#modalUSer').on('hidden.bs.modal', function () {
        $("#errorSalirViajeServidor").addClass("hide");
        $("#loadingSpinnerExit").addClass("hide");
    })
});