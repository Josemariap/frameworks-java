snoopooling.ui.travelDelete = (function () {

    var urlTravel = snoopooling.urlBase() + snoopooling.service.uriTravel();

    var $divErrorServidor = $("#errorServidorEditar");

    var traverPlaces = document.getElementById('placesTravel').value;
    var travelDestination = document.getElementById('destinationTravel').value;

    function init() {
        bindearEventosABotonEditar();
        setearParametros();
    }

    function setearParametros(){

        var selectedPlace = "#place_" + traverPlaces;
        $(selectedPlace).attr('selected', 'selected');
        if(travelDestination==="Sarmiento 1230"){
            $("#plata-bs").attr('selected', 'selected');
        }
    }

    function bindearEventosABotonEditar() {
        $("#botonEditarViaje").on("click", function () {
            $divErrorServidor.addClass("hide");
            $spinner.removeClass("hide");

            var date = $("#date").val();
            var dateArray = date.split("T");
            var origin = "Calle 48 nro 804";
            var coorOrigin = "-34.917445,-57.9575959";
            var destination = "Sarmiento 1230";
            var coorDestination = "-34.605187,-58.3862633";
            var places = $("#places").val();

            if (moment(dateArray[0], 'yyyy-MM-dd').isValid() && moment(dateArray[1], "HH:mm").isValid()) {

                if ($("#origin-destination").val() === "Buenos Aires - La plata") {
                    origin = "Sarmiento 1230";
                    coorOrigin = "-34.605187,-58.3862633";
                    destination = "Calle 48 nro 804";
                    coorDestination = "-34.917445,-57.9575959";
                }
                var travelId = document.getElementById('idTravel').value;

                editarViaje(origin, coorOrigin, destination, coorDestination, date, places, travelId)

            } else {
                mostrarMensajeFechaErronea()
            }

        })
    }

    function editarViaje(origin, coorOrigin, destination, coorDestination, date, places, travelId) {



        var travel = {
            "id": travelId,
            "date": date,
            "origin": origin,
            "destination": destination,
            "places": places,
            "coorOrigin": coorOrigin,
            "coorDestination": coorDestination
        };

        snoopooling.service.travel.editar(travel).done(function () {
            redireccionarAUrlRetorno(travelId)
        }).fail(function (error) {
            mostrarMensajeErrorServidor(error.responseJSON.message)
        })
    }

    function redireccionarAUrlRetorno(travelId) {
        $divErrorServidor.addClass("hide");
        $spinner.addClass("hide");
        window.location.href = urlTravel + "/" + travelId;
    }

    function mostrarMensajeErrorServidor(error) {
        $divErrorServidor.removeClass("hide");
        $spinner.addClass("hide");
        document.getElementById('errorServidorEditar').innerHTML = error;
    }

    function mostrarMensajeFechaErronea() {
        $divFechaErronea.removeClass("hide");
    }

    return {
        init: init
    };

})();

$(document).on("ready", function () {
    snoopooling.ui.travelDelete.init();

    $('#driverModal').on('hidden.bs.modal', function () {
        $("#errorServidorEditar").addClass("hide");
        $("#loadingSpinnerEditTravel").addClass("hide");
    })
});