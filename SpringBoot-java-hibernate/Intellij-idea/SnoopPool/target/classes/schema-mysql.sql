CREATE TABLE `persons` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) DEFAULT NULL,
  `MAIL` varchar(50) DEFAULT NULL,
  `AVATAR` varchar(255) NOT NULL,
  `ID_WORKPLACE` int(11) NOT NULL
)ENGINE = InnoDB;

CREATE TABLE `persons_travel` (
  `ID` int(11) NOT NULL,
  `ID_PERSON` int(11) DEFAULT NULL,
  `ID_TRAVEL` int(11) DEFAULT NULL
)ENGINE = InnoDB;
CREATE TABLE `travel` (
  `ID` int(11) NOT NULL,
  `DATE` datetime DEFAULT NULL,
  `ORIGIN` varchar(100) DEFAULT NULL,
  `DESTINATION` varchar(100) DEFAULT NULL,
  `ID_PERSON` int(11) DEFAULT NULL,
  `PLACES` int(11) DEFAULT NULL,
  `COOR_ORIGIN` varchar(250) DEFAULT NULL,
  `COOR_DESTINATION` varchar(250) DEFAULT NULL
)ENGINE = InnoDB;

ALTER TABLE `persons`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `persons_travel`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_PERSON` (`ID_PERSON`),
  ADD KEY `ID_TRAVEL` (`ID_TRAVEL`);

ALTER TABLE `travel`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_USER` (`ID_PERSON`);

ALTER TABLE `persons`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `persons_travel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `travel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `persons_travel`
  ADD CONSTRAINT `persons_travel_ibfk_1` FOREIGN KEY (`ID_TRAVEL`) REFERENCES `travel` (`ID`),
  ADD CONSTRAINT `persons_travel_ibfk_2` FOREIGN KEY (`ID_PERSON`) REFERENCES `persons` (`ID`);


ALTER TABLE `travel`
  ADD CONSTRAINT `travel_ibfk_1` FOREIGN KEY (`ID_PERSON`) REFERENCES `persons` (`ID`);

INSERT INTO `persons` (`ID`, `NAME`, `MAIL`, `AVATAR`, `ID_WORKPLACE`) VALUES
(100, 'Jose Maria Pico', 'jose.pico@snoopconsulting.com', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/19702032_114894175797584_2110401105489943055_n.jpg?oh=250ae1b310a8c420a9b93302d4a76506&oe=5A12226B', 0),
(200, 'Maximiliano Caba', 'maximiliano.caba@snoopconsulting.com', 'https://scontent.xx.fbcdn.net/v/t1.0-1/c8.0.50.50/p50x50/19420628_111906179428584_2664364349083929277_n.jpg?oh=aaf74b093767351bf99c410980a5e766&oe=5A55CB89', 0);


INSERT INTO `travel` (`ID`, `DATE`, `ORIGIN`, `DESTINATION`, `ID_PERSON`, `PLACES`, `COOR_ORIGIN`, `COOR_DESTINATION`) VALUES
  (3, '2017-11-21 15:15:00', 'sarmiento 1230', 'rivadavia 1200', 100, 4, '-34.605187,-58.3862633', '-34.6096629,-58.3869025'),
  (4, '2017-11-22 11:11:00', 'Sarmiento 1230', 'Calle 48 nro 804', 200, 4, '-34.605187,-58.3862633', '-34.917445,-57.9575959'),
  (5, '2017-11-20 00:00:00', 'Sarmiento 1230', 'Cuenca 100', 100, 4, '-34.6051826,-58.3862633', '-34.6305345,-58.4763787');

INSERT INTO `persons_travel` (`ID`, `ID_PERSON`, `ID_TRAVEL`) VALUES
  (1, 200, 3),
  (2, 100, 4);