package com.mitocode.beans;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class CodeBeanPostProcessor implements BeanPostProcessor  {

	@Override
	public Object postProcessAfterInitialization(Object bean, String nombreBean) throws BeansException {
		System.out.println("Despues de la inicializacion del bean: " + nombreBean); //nombreBean toma el id del bean segun el appcontext.xml, y el bean del xml tambien
		return bean;
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String nombreBean) throws BeansException {
		System.out.println("Antes de la inicializacion del bean: " + nombreBean); //nombreBean toma el id del bean segun el appcontext.xml
		return bean;
	}

}
