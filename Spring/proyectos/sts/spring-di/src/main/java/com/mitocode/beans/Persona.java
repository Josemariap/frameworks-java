package com.mitocode.beans;

import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

public class Persona {
 //@Autowired //Esta propiedad la colocara de forma automatica y con @Qualifier indicamos el id del bean que queremos que le coloque a la propiedad, indicar en el xml la siguiente clase para que reconozca la annotation:org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor
 //@Qualifier("colocamosname") //Al bean de World del xml que queremos que le asigne a la propiedad md, le colocamos la propiedad: <qualifier value="colocamosname"> y sabra que bean asgnarles si es que tenemos muchos bean de Mundo // si esto da erro revisar si falta el namespace para estas propiedades en el xml
   private World md; 
   private String nom;
   private int edad;
   private String pass;
   private List<Curso> cursos;
   
   public Persona(String nom, int edad, String pass) {
		this.nom = nom;
		this.edad = edad;
		this.pass = pass;
	    
   }

public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}

public int getEdad() {
	return edad;
}

public void setEdad(int edad) {
	this.edad = edad;
}

public String getPass() {
	return pass;
}

public void setPass(String pass) {
	this.pass = pass;
}



public World getMd() {
	return md;
}

@Required  //La propiedad md es obligatoria, indicar en el beans.xml este bean para que reconozca esta annotation:
public void setMd(World md) {
	this.md = md;
}



public List<Curso> getCursos() {
	return cursos;
}

public void setCursos(List<Curso> cursos) {
	this.cursos = cursos;
}

public void iterarCursos(){
	Curso c;
	Iterator it = cursos.iterator();
	while (it.hasNext() ) {
        c = (Curso)it.next();
        System.out.println(c.getDescripcion());
 }
}

@Override
public String toString() {
	return "Persona --> mundo=" + md.getSaludo() + ", nombre=" + nom + ", edad=" + edad + ", password=" + pass + ", cursos=" + cursos.get(0).getDescripcion() + " ";
}


//Ciclo de vida del bean , se pueden usar annotation o indicar en el beans.xml, aca veremos ambos
//Esta annotation debe estar acompañada en el beans.xml por el siguiente bean para que funcione, ya que no es propia de spring: <bean class="org.springframework.context.annotation.CommonAnnotationBeanPostProcessor"></bean>
//Tambien se pueden implemetnar las interfaces InitializingBean y DisposableBean sobreescribiendo los metodos para que realicen la misma funcion 
//La ultima opcion es crear una clase que implemente BeanPostProcessor y sobreescribir sus dos metodos after y before con la logica correspondiente, agregar el bean de esta clase al applicatiocontext.xml
@PostConstruct
private void postContruct(){
	System.out.println("Este metodo ejecuta postContruct despues de instanciar el bean en memoria ");
}


private void init(){
	System.out.println("Este metodo se ejecuta antes de iniciar el bean Persona pero despues del postContruct");
}

private void destroy(){
	System.out.println("Este metodo se ejecuta justo antes de que el bean sea destruido");
}
	
   
}
