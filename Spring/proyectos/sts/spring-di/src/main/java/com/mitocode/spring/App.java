package com.mitocode.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mitocode.beans.Appconfig;
import com.mitocode.beans.Persona;
import com.mitocode.beans.World;

public class App {

	public static void main(String[] args) {
		        /*Con aplication context spring hace la carga de todos los beans en memoria, segun los scopes pero si al bean le indico: lazy-init="true" solo 
		          instanciara el bean cuando se haga el getBean() */
				ApplicationContext appContext = new ClassPathXmlApplicationContext("com/mitocode/xml/beans.xml");  //usando xml 
				ApplicationContext appContext2 = new AnnotationConfigApplicationContext(Appconfig.class);  //usando un clase y annotations
                 
				
				World w = (World) appContext.getBean("mundo");   //id del bean del xml 
				World w2 = (World) appContext2.getBean("world");  //nombre de la clase
				
				System.out.println(w.getSaludo());
				
				w2.setSaludo("Esto anda?-.-!!");
				System.out.println(w2.getSaludo());
				
				/***********************/
				
				Persona p = (Persona) appContext.getBean("persona"); // se puede llamar por medio de alias del xml (beanPersona)
				System.out.println(p.toString());
				p.iterarCursos();
				
				
				((ConfigurableApplicationContext) appContext).close(); //cerramos el appContext
	}

}
