package com.mitocode.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Appconfig {

	 @Bean
	 public World world(){
		 return new World();
	 }
}
