package com.mitocode.beans;

import org.springframework.stereotype.Component;

//Defino el stereotipo para no tener que crear los bean en el appContext, no olvidar hacer el scan de paquetes en el aapContext para que cree el bean automaticamente
//@Component    
public class World {
 
	private String saludo;

	public String getSaludo() {
		return saludo;
	}

	public void setSaludo(String saludo) {
		this.saludo = saludo;
	}
	
	
}
