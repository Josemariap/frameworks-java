package com.mitocode.spring;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController {

	@RequestMapping("irHola")
	public ModelAndView redireccion(){
		ModelAndView mv = new ModelAndView();
		String msj="Soy una variable del controller";
		mv.addObject("mensaje", msj);
		mv.setViewName("hello");
		return mv;
	}
}
